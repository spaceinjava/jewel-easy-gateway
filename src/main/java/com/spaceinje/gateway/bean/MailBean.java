/**
 * 
 */
package com.spaceinje.gateway.bean;

import java.util.Map;

/**
 * @author Lakshmi Kiran
 * @implNote contains the mail properties which were used to send email
 * @version 1.0
 */
public class MailBean {

	private String username;

	private String subject;

	private String to;

	private String filepath;

	private String license_type;

	private String license_start_date;

	private String license_end_date;

	private int license_active_days;

	private String tenant_password;
	
	private Map<String, Object> model;
	
	private String template_name;
	
	private boolean has_attachement;
	
	private String media_type;
	
	private String file_name;

	
	
	/**
	 * @return the has_attachement
	 */
	public boolean isHas_attachement() {
		return has_attachement;
	}

	/**
	 * @param has_attachement the has_attachement to set
	 */
	public void setHas_attachement(boolean has_attachement) {
		this.has_attachement = has_attachement;
	}

	/**
	 * @return the media_type
	 */
	public String getMedia_type() {
		return media_type;
	}

	/**
	 * @param media_type the media_type to set
	 */
	public void setMedia_type(String media_type) {
		this.media_type = media_type;
	}

	/**
	 * @return the file_name
	 */
	public String getFile_name() {
		return file_name;
	}

	/**
	 * @param file_name the file_name to set
	 */
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the filepath
	 */
	public String getFilepath() {
		return filepath;
	}

	/**
	 * @param filepath the filepath to set
	 */
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	/**
	 * @return the license_type
	 */
	public String getLicense_type() {
		return license_type;
	}

	/**
	 * @param license_type the license_type to set
	 */
	public void setLicense_type(String license_type) {
		this.license_type = license_type;
	}

	/**
	 * @return the license_start_date
	 */
	public String getLicense_start_date() {
		return license_start_date;
	}

	/**
	 * @param license_start_date the license_start_date to set
	 */
	public void setLicense_start_date(String license_start_date) {
		this.license_start_date = license_start_date;
	}

	/**
	 * @return the license_end_date
	 */
	public String getLicense_end_date() {
		return license_end_date;
	}

	/**
	 * @param license_end_date the license_end_date to set
	 */
	public void setLicense_end_date(String license_end_date) {
		this.license_end_date = license_end_date;
	}

	/**
	 * @return the license_active_days
	 */
	public int getLicense_active_days() {
		return license_active_days;
	}

	/**
	 * @param license_active_days the license_active_days to set
	 */
	public void setLicense_active_days(int license_active_days) {
		this.license_active_days = license_active_days;
	}

	/**
	 * @return the tenant_password
	 */
	public String getTenant_password() {
		return tenant_password;
	}

	/**
	 * @param tenant_password the tenant_password to set
	 */
	public void setTenant_password(String tenant_password) {
		this.tenant_password = tenant_password;
	}

	/**
	 * @return the model
	 */
	public Map<String, Object> getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(Map<String, Object> model) {
		this.model = model;
	}

	/**
	 * @return the template_name
	 */
	public String getTemplate_name() {
		return template_name;
	}

	/**
	 * @param template_name the template_name to set
	 */
	public void setTemplate_name(String template_name) {
		this.template_name = template_name;
	}

	@Override
	public String toString() {
		return "MailBean [username=" + username + ", subject=" + subject + ", to=" + to + ", filepath=" + filepath
				+ ", license_type=" + license_type + ", license_start_date=" + license_start_date
				+ ", license_end_date=" + license_end_date + ", license_active_days=" + license_active_days
				+ ", tenant_password=" + tenant_password + ", model=" + model + ", template_name=" + template_name
				+ ", has_attachement=" + has_attachement + ", media_type=" + media_type + ", file_name=" + file_name
				+ "]";
	}
	
}
