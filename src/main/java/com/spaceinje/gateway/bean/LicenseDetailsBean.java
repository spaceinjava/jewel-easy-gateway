package com.spaceinje.gateway.bean;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class LicenseDetailsBean {

	private String id;

	private Boolean isDeleted;

	private String tenantCode;

	private Boolean isLicenseFree;

	private String licenseType;

	@DateTimeFormat
	private LocalDate startDate;

	@DateTimeFormat
	private LocalDate endDate;

	private Boolean isLicenseActive;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Boolean getIsLicenseFree() {
		return isLicenseFree;
	}

	public void setIsLicenseFree(Boolean isLicenseFree) {
		this.isLicenseFree = isLicenseFree;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsLicenseActive() {
		return isLicenseActive;
	}

	public void setIsLicenseActive(Boolean isLicenseActive) {
		this.isLicenseActive = isLicenseActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "LicenseDetailsBean [id=" + id + ", tenantCode=" + tenantCode + ", isLicenseFree=" + isLicenseFree
				+ ", licenseType=" + licenseType + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", isLicenseActive=" + isLicenseActive + ", isDeleted=" + isDeleted + "]";
	}

}
