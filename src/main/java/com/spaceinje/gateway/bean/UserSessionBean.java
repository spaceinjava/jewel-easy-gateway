package com.spaceinje.gateway.bean;

import java.time.LocalDateTime;

public class UserSessionBean {

	private String id;

	private Boolean isDeleted;

	private String authCode;

	private String authToken;

	private Integer authCodeExpiry;

	private Integer authTokenExpiry;

	private Boolean isAuthCodeActive;

	private Boolean isAuthTokenActive;

	private LocalDateTime authorizedAt;

	private LocalDateTime authenticatedAt;

	private UserInfoBean userInfo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public Integer getAuthCodeExpiry() {
		return authCodeExpiry;
	}

	public void setAuthCodeExpiry(Integer authCodeExpiry) {
		this.authCodeExpiry = authCodeExpiry;
	}

	public Integer getAuthTokenExpiry() {
		return authTokenExpiry;
	}

	public void setAuthTokenExpiry(Integer authTokenExpiry) {
		this.authTokenExpiry = authTokenExpiry;
	}

	public Boolean getIsAuthCodeActive() {
		return isAuthCodeActive;
	}

	public void setIsAuthCodeActive(Boolean isAuthCodeActive) {
		this.isAuthCodeActive = isAuthCodeActive;
	}

	public Boolean getIsAuthTokenActive() {
		return isAuthTokenActive;
	}

	public void setIsAuthTokenActive(Boolean isAuthTokenActive) {
		this.isAuthTokenActive = isAuthTokenActive;
	}

	public LocalDateTime getAuthorizedAt() {
		return authorizedAt;
	}

	public void setAuthorizedAt(LocalDateTime authorizedAt) {
		this.authorizedAt = authorizedAt;
	}

	public LocalDateTime getAuthenticatedAt() {
		return authenticatedAt;
	}

	public void setAuthenticatedAt(LocalDateTime authenticatedAt) {
		this.authenticatedAt = authenticatedAt;
	}

	public UserInfoBean getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfoBean userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public String toString() {
		return "UserSessionBean [id=" + id + ", isDeleted=" + isDeleted + ", authCode=" + authCode + ", authToken="
				+ authToken + ", authCodeExpiry=" + authCodeExpiry + ", authTokenExpiry=" + authTokenExpiry
				+ ", isAuthCodeActive=" + isAuthCodeActive + ", isAuthTokenActive=" + isAuthTokenActive
				+ ", authorizedAt=" + authorizedAt + ", authenticatedAt=" + authenticatedAt + ", userInfo=" + userInfo
				+ "]";
	}

}
