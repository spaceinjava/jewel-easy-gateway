///**
// * 
// */
//package com.spaceinje.gateway.serviceImpl.nosql;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.annotation.CachePut;
//import org.springframework.stereotype.Service;
//
//import com.spaceinje.gateway.model.nosql.UserInfo;
//import com.spaceinje.gateway.repo.nosql.UserInfoRepo;
//import com.spaceinje.gateway.service.nosql.UserInfoService;
//
///**
// * @author Lakshmi Kiran
// *
// */
//@Service("userInfoService")
//public class UserInfoServiceImpl implements UserInfoService {
//
//	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);
//	
//	@Autowired
//	private UserInfoRepo userInfoRepo;
//	
//
//	@CachePut(value = "USERINFO_CACHE", key = "#hash")
//	@Override
//	public UserInfo findByHash(String hash) {
//		if(log.isDebugEnabled())
//			log.debug("Entering findByTenantHash service");
//		UserInfo info = null;
//		try {
//			log.info("retrieving from repo");
//			info = userInfoRepo.findByHash(hash);
//			if(log.isDebugEnabled())
//				log.debug("Exiting findByTenantHash service");
//			return info;
//		} catch (Exception e) {
//			log.info("exception while retrieving admin");
//			if(log.isDebugEnabled())
//				log.debug("Exiting findByTenantHash service");
//			return null;
//		}
//	}
//
//
//}
