/**
 * 
 */
package com.spaceinje.gateway.serviceImpl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.generator.SecretKeyGenerator;
import com.spaceinje.gateway.model.nosql.UserInfo;
import com.spaceinje.gateway.repo.nosql.UserInfoRepo;
import com.spaceinje.gateway.service.CommonService;
import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;
import com.spaceinje.gateway.validators.BasicValidator;

import brave.Tracer;

/**
 * @author Lakshmi Kiran
 * @implNote service class which implements common methods used by admin and
 *           tenant
 * @version 1.0
 */
@Service("commonService")
public class CommonServiceImpl implements CommonService {

	private static final Logger log = LoggerFactory.getLogger(CommonServiceImpl.class);

	@Autowired
	private UserInfoRepo userInfoRepo;

	@Autowired
	private SecretKeyGenerator secretKeyGenerator;

	@Autowired
	private BasicValidator basicValidator;

	@Autowired
	private Tracer tracer;

	@Autowired
	private GatewayConstants constants;

	/**
	 * method to reset the password
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public ResponseCachet resetPassword(SigninUtil util, boolean is_tenant) {
		if (log.isDebugEnabled())
			log.debug("Entering resetPassword service");
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.info("querying database with user name");
			UserInfo user;
			if (is_tenant)
				user = userInfoRepo.findByTenantCode(util.getUserName());
			else
				user = userInfoRepo.findByAdminCode(util.getUserName());
			if (null == user) {
				log.info("No user exists with user code : " + util.getUserName());
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No user exists with user code : " + util.getUserName());

				if (log.isDebugEnabled())
					log.debug("Exiting resetPassword service");
				return cachet;
			}
			log.info("validating the password match");
			if (!basicValidator.validateLoginPasswordMatch(util.getPassword(), user.getPassword())) {
				user.setLogin_attempts(user.getLogin_attempts() + 1);
				userInfoRepo.save(user);
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("Invalid password, you have "
						+ (Integer.valueOf(constants.getMax_login_attempts()) - user.getLogin_attempts())
						+ " attempts to login");

				if (log.isDebugEnabled())
					log.debug("Exiting resetPassword service");
				return cachet;
			}
			log.info("validating the new password");
			if (!basicValidator.isPasswordValid(util.getNewPassword())) {
				log.info("invalid password");
				cachet.setMessage("invalid password : It contains at least 8 characters and at most 20 characters.\r\n"
						+ "     *           It contains at least one digit.\r\n"
						+ "     *           It contains at least one upper case alphabet.\r\n"
						+ "     *           It contains at least one lower case alphabet.\r\n"
						+ "     *           It contains at least one special character which includes !@#$%&*()-+=^.\r\n"
						+ "     *           It does not contain any white space.");
				cachet.setStatus(constants.getStatus_message_failure());
				if (log.isDebugEnabled())
					log.debug("Exiting resetPassword service");
				return cachet;
			}
			log.info("new password is valid");
			log.info("encrypting and updating the password");
			user.setPassword(secretKeyGenerator.generateHash(constants.getStatus_response_type_password(),
					util.getNewPassword()));
			user.setLogin_status(constants.getLogin_status_activated());
			log.info("saving new password");
			userInfoRepo.save(user);
			log.info("password reset successful");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("password reset successful, please login to continue");
			if (log.isDebugEnabled())
				log.debug("Exiting resetPassword service");
			return cachet;

		} catch (NumberFormatException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("Exception while resetting the password", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			if (log.isDebugEnabled())
				log.debug("Exiting resetPassword service");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while resetting the password", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("Exception while resetting the password");
			if (log.isDebugEnabled())
				log.debug("Exiting resetPassword service");
			return cachet;
		}
	}

}
