package com.spaceinje.gateway.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.DocumentException;
import com.spaceinje.gateway.bean.MailBean;
import com.spaceinje.gateway.bean.UserInfoBean;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.generator.EmailGenerator;
import com.spaceinje.gateway.generator.PDFGenerator;
import com.spaceinje.gateway.generator.SecretKeyGenerator;
import com.spaceinje.gateway.model.LicenseDetails;
import com.spaceinje.gateway.model.UserInfo;
import com.spaceinje.gateway.repository.UserInfoRepository;
import com.spaceinje.gateway.service.UserInfoService;
import com.spaceinje.gateway.specifications.CommonSpecs;
import com.spaceinje.gateway.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService {

	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EmailGenerator emailGenerator;

	@Autowired
	private Tracer tracer;

	@Autowired
	private GatewayConstants constants;

	@Autowired
	private SecretKeyGenerator secretKeyGenerator;

	@Autowired
	private PDFGenerator pdfGenerator;

	@Autowired
	private UserInfoRepository userRepository;

	/**
	 * service to save user
	 * 
	 * @implNote while saving user, a pdf file will be generated with admin hash and
	 *           secret and will be sent to mail
	 * 
	 * @return {@value ResponseCachet}
	 */

	@Transactional
	@Override
	public ResponseCachet saveOrUpdateUser(UserInfoBean bean) {
		ResponseCachet<UserInfoBean> cachet = new ResponseCachet<UserInfoBean>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether user exists with given email or mobile");
			Optional<UserInfo> retrievedUser = userRepository
					.findOne(CommonSpecs.findByEmailOrMobileSpec(bean.getEmail(), bean.getMobile()));
			if (retrievedUser.isPresent()) {
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("user already exists with email or mobile");
				log.debug("user already exists with email or mobile");
				return cachet;
			}
			UserInfo userInfo = modelMapper.map(bean, UserInfo.class);
			userInfo.setLoginAttempts(0);
			userInfo.setLoginStatus(constants.getLogin_status_new());
			String temporaryPassword = userInfo.getPassword();
			if (userInfo.getIsTenant()) {
				generateHashAndSecret(userInfo);
				userInfo.setTenantCode(generateTenantCode());
				// setting tenant emailId as login userName
				userInfo.setUserName(userInfo.getEmail());
				temporaryPassword = secretKeyGenerator.generatePassword() + constants.getPassword_suffix();
				generateLicenseDetails(userInfo);
				tenantCreationMailSender(userInfo, temporaryPassword);
				
				/*
					ResponseCachet<?> mailSenderResponse = tenantCreationMailSender(userInfo, temporaryPassword);
					if (mailSenderResponse.getStatus().equals(constants.getStatus_message_failure())) {
						cachet.setMessage("exception while sending email");
						cachet.setStatus(constants.getStatus_message_failure());
						return cachet;
					}
				*/
				 
			}
			userInfo.setPassword(
					secretKeyGenerator.generateHash(constants.getStatus_response_type_password(), temporaryPassword));
			userInfo = userRepository.save(userInfo);
			log.debug("successfully saved user : {}");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("tenant created successfully with code : " + userInfo.getTenantCode());
			cachet.setData(modelMapper.map(userInfo, UserInfoBean.class));
			return cachet;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("exception while generating password hash/secret/password : {} ", e.getMessage());
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("exception while generating password hash/secret/password");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/*
	 * generate hash code and secret while creating tenant
	 */
	private void generateHashAndSecret(UserInfo userInfo) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String hash = secretKeyGenerator.generateHash(constants.getStatus_response_type_code(),
				secretKeyGenerator.generatePassword());
		String secret = secretKeyGenerator.generateHash(constants.getStatus_response_type_token(),
				secretKeyGenerator.generatePassword());
		userInfo.setHash(hash);
		userInfo.setSecret(secret);
	}

	/*
	 * generate tenant code while creating tenant
	 */
	private String generateTenantCode() {
		return constants.getTenant_id_prefix() + RandomStringUtils.randomNumeric(constants.getId_length());
	}

	/*
	 * generate license details while creating tenant
	 */
	private void generateLicenseDetails(UserInfo userInfo) {
		LicenseDetails licenseDetails = userInfo.getLicenseDetails();
		if (ObjectUtils.isEmpty(licenseDetails)) {
			licenseDetails = new LicenseDetails();
			licenseDetails.setIsDeleted(false);
			licenseDetails.setIsLicenseActive(true);
			licenseDetails.setIsLicenseFree(false);
			licenseDetails.setLicenseType("Free");
			LocalDate licenseStart = LocalDate.now(ZoneId.of("Asia/Kolkata"));
			// adding 6 months to license start date to obtain license end date
			LocalDate licenseEnd = licenseStart.plusMonths(constants.getLicenseMonths());
			licenseDetails.setStartDate(licenseStart);
			licenseDetails.setEndDate(licenseEnd);
			// licenseDetails.setActiveDays((int) ChronoUnit.DAYS.between(licenseEnd, licenseStart));
		}
		licenseDetails.setTenantCode(userInfo.getTenantCode());
		licenseDetails.setUserInfo(userInfo);
		userInfo.setLicenseDetails(licenseDetails);
	}

	private ResponseCachet<?> tenantCreationMailSender(UserInfo user, String passcode) {
		ResponseCachet<?> response = new ResponseCachet<>();
		String filePass = user.getFirstName().toLowerCase().substring(0, 4)
				+ user.getMobile().substring(user.getMobile().length() - 4, user.getMobile().length());
		try {
			String filepath = pdfGenerator.createPDF(user.getHash(), user.getSecret(),
					user.getFirstName().replace(" ", ""), user.getTenantCode(), filePass);

			log.debug("preparing the mail bean props to send mail to admin");
			MailBean mail = new MailBean();
			mail.setFilepath(filepath);
			mail.setTo(user.getEmail());
			mail.setUsername(user.getFirstName());
			mail.setHas_attachement(Boolean.TRUE);
			mail.setFile_name("JE_" + filepath.split("JE_")[1]);
			mail.setMedia_type(MediaType.APPLICATION_PDF_VALUE);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("username", mail.getUsername());
			model.put("password", passcode);
			model.put("code", user.getTenantCode());

			mail.setSubject(constants.getTenant_email_subject());
			mail.setTemplate_name("tenant_registration.html");
			model.put("license_start_date", user.getLicenseDetails().getStartDate().toString());
			model.put("license_end_date", user.getLicenseDetails().getEndDate().toString());
			// model.put("license_active_days", user.getLicenseDetails().getActiveDays());
			model.put("license_type", user.getLicenseDetails().getLicenseType());

			mail.setModel(model);

			log.debug("calling email generator service");
			response = emailGenerator.sendMail(mail);

			// deleting the generated pdf files
			File file = new File(filepath);
			boolean delete = file.delete();
			filepath = filepath.replace("JE_", "");
			File enc_file = new File(filepath);
			boolean enc_delete = enc_file.delete();
			if (delete && enc_delete)
				log.debug("pdf files deleted successfully");

			return response;
		} catch (DocumentException | IOException e) {
			log.error("DocumentException :: IOException :: exception while creating pdf", e);
			response.setMessage("exception while creating pdf");
			response.setStatus(constants.getStatus_message_failure());
			return response;
		}

	}

	@Transactional
	@Override
	public ResponseCachet deleteUsers(Set<String> userIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("retrieving all user records based on input ids");
			List<UserInfo> user = userRepository.findAllById(userIds);
			user.stream().forEach(ele -> {
				ele.setIsDeleted(true);
				ele.getLicenseDetails().setIsDeleted(true);
			});
			// updating isDeleted to true in both userInfo and license details, to all the
			// given userIds
			userRepository.saveAll(user);
			log.debug("deleted user(s) successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("deleted user(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting users: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
