///**
// * 
// */
//package com.spaceinje.gateway.serviceImpl;
//
//import java.util.Date;
//
//import org.joda.time.DateTime;
//import org.joda.time.Seconds;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.annotation.CachePut;
//import org.springframework.cache.annotation.Caching;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import com.spaceinje.gateway.constants.GatewayConstants;
//import com.spaceinje.gateway.model.SessionTracker;
//import com.spaceinje.gateway.model.nosql.UserInfo;
//import com.spaceinje.gateway.repo.nosql.UserInfoRepo;
//import com.spaceinje.gateway.repository.SessionTrackerRepo;
//import com.spaceinje.gateway.service.nosql.SessionCachetService;
//import com.spaceinje.gateway.util.ResponseCachet;
//
//import brave.Tracer;
//
//@Service("sessionCachetService")
//public class SessionCachetServiceImpl implements SessionCachetService {
//
//	private static final Logger log = LoggerFactory.getLogger(SessionCachetServiceImpl.class);
//
//	@Autowired
//	private SessionTrackerRepo sessionCachetRepo;
//
//	@Autowired
//	Tracer tracer;
//
//	@Autowired
//	GatewayConstants constants;
//
//	@Autowired
//	UserInfoRepo userInfoRepo;
//
//	@CachePut(value = "SESSION_CACHE", key = "#code")
//	@Override
//	public SessionTracker findByCode(String code) {
//		if (log.isDebugEnabled())
//			log.debug("Entering findByCode service");
//		try {
//			SessionTracker cachet = sessionCachetRepo.findByAuthCodeAndIsCodeActiveTrue(code);
//			return cachet;
//		} catch (Exception e) {
//			log.error("exception while retrieving session cachet");
//			return null;
//		}
//	}
//
//	@CachePut(value = "SESSION_CACHE", key = "#auth_token")
//	@Override
//	public SessionTracker findByAuthToken(String auth_token) {
//		if (log.isDebugEnabled())
//			log.debug("Entering findByCode service");
//		try {
//			SessionTracker cachet = sessionCachetRepo.findByAuthTokenAndIsTokenActiveTrue(auth_token);
//			return cachet;
//		} catch (Exception e) {
//			log.error("exception while retrieving session cachet");
//			return null;
//		}
//	}
//
//	@Caching(put = { @CachePut(value = "SESSION_CACHE", key = "#cachet.getCode()") })
//	@Override
//	public SessionTracker updateSession(SessionTracker cachet) {
//		if (log.isDebugEnabled())
//			log.debug("Entering findByCode service");
//		try {
//			SessionTracker sessionCachet = sessionCachetRepo.save(cachet);
//			return sessionCachet;
//		} catch (Exception e) {
//			log.error("exception while updating session cachet");
//			return null;
//		}
//	}
//
//	/**
//	 * service to retrieve the session based on auth code or token
//	 * 
//	 * @param token - authentication token
//	 * 
//	 * @return {@value SessionCachet}
//	 */
//	@Caching(put = { @CachePut(value = "RESPONSE_CACHE", key = "#token") })
//	@Override
//	public ResponseCachet<SessionTracker> findSessionByAuth(String token) {
//		if (log.isDebugEnabled())
//			log.debug("Entering findSessionByAuth service");
//		ResponseCachet<SessionTracker> cachet = new ResponseCachet<SessionTracker>();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//
//		log.info("retrieving session with token");
//		SessionTracker session = sessionCachetRepo.findByAuthTokenAndIsTokenActiveTrue(token);
//		if (null == session) {
//			log.info("no session found with auth token : " + token);
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("no session found with auth token : " + token);
//
//			if (log.isDebugEnabled())
//				log.debug("Exiting findSessionByAuth service");
//			return cachet;
//		}
//		log.info("checking whether session is active or not");
//		if (!isSessionActive(session)) {
//			log.info("Inactive session, please authorise or authenticate to continue");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("Inactive session, please authenticate to continue");
//
//			if (log.isDebugEnabled())
//				log.debug("Exiting findSessionByAuth service");
//			return cachet;
//		}
//
//		log.info("checking the role access");
//		UserInfo info = null;
//		try {
//			info = userInfoRepo.findByHash(session.getHash());
//		} catch (Exception e) {
//			log.error("error while retrieving user info", e);
//			cachet.setStatus(constants.getStatus_message_exception());
//			cachet.setMessage("error while retrieving user info");
//
//			if (log.isDebugEnabled())
//				log.debug("Exiting findSessionByAuth service");
//			return cachet;
//		}
//		if (null == info) {
//			log.info("No info found with tenant or admin");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("No info found with tenant or admin");
//
//			if (log.isDebugEnabled())
//				log.debug("Exiting findSessionByAuth service");
//			return cachet;
//		}
//
//		log.info("active session, retrieved successfully with auth code");
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setMessage("active session, retrieved successfully with auth code");
//
//		if (log.isDebugEnabled())
//			log.debug("Exiting findSessionByAuth service");
//		cachet.setData(session);
//		return cachet;
//	}
//
//	/**
//	 * service to check whether the session is active or not
//	 * 
//	 * @param tracker - SessionTracker
//	 * 
//	 * @return {@value Boolean.TRUE, Boolean.FALSE}
//	 */
//	public boolean isSessionActive(SessionTracker tracker) {
//		if (log.isDebugEnabled())
//			log.debug("Entering  inSessionActive  service");
//
//		boolean active = Boolean.TRUE;
//		log.info("retrieving all the active sessions");
//		// checking for the sessions which crossed the time limit and updating
//		// getting the session last updated date time and current date time
//
//		if (!StringUtils.isEmpty(tracker.getAuthCode())) {
//			DateTime active_auth_dateTime = new DateTime(tracker.getAuthorizedDate().getTime());
//			Seconds seconds = Seconds.secondsBetween(active_auth_dateTime, new DateTime());
//			if (seconds.getSeconds() >= constants.getAuth_code_expiry_in_seconds()) {
//				log.info("deactivating the session and saving to db");
//				tracker.setIsCodeActive(Boolean.FALSE);
//				tracker.setAuthorizedDate(new Date());
//				sessionCachetRepo.save(tracker);
//				active = Boolean.FALSE;
//			}
//		}
//		if (!StringUtils.isEmpty(tracker.getAuthToken())) {
//			DateTime active_auth_dateTime = new DateTime(tracker.getAuthenticatedDate().getTime());
//			Seconds seconds = Seconds.secondsBetween(active_auth_dateTime, new DateTime());
//			if (seconds.getSeconds() >= constants.getAuth_token_expiry_in_seconds()) {
//				log.info("deactivating the session and saving to db");
//				tracker.setIsCodeActive(Boolean.FALSE);
//				tracker.setAuthenticatedDate(new Date());
//				sessionCachetRepo.save(tracker);
//				active = Boolean.FALSE;
//			}
//		}
//
//		if (log.isDebugEnabled())
//			log.debug("Exiting  inSessionActive  service");
//		return active;
//	}
//
//	/**
//	 * service to inactivate session
//	 * 
//	 * @param auth_token - authentication token
//	 * 
//	 * @return {@value ResponseCachet}
//	 */
//	@SuppressWarnings("rawtypes")
//	public ResponseCachet inactivateSession(String auth_token) {
//		if (log.isDebugEnabled())
//			log.debug("Entering  inactivateSession  service");
//		ResponseCachet cachet = new ResponseCachet();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//		if (StringUtils.isEmpty(auth_token)) {
//			log.info("auth token can't be empty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("auth token can't be empty");
//
//			if (log.isDebugEnabled())
//				log.debug("Exiting inactivateSession service");
//			return cachet;
//		}
//		log.info("retrieving the session with auth token");
//		SessionTracker session = sessionCachetRepo.findByAuthTokenAndIsTokenActiveTrue(auth_token);
//		if (null == session) {
//			log.info("no session found with auth token : " + auth_token);
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("no session found with auth token : " + auth_token);
//
//			if (log.isDebugEnabled())
//				log.debug("Exiting inactivateSession service");
//			return cachet;
//		}
//		log.info("in activating session");
//		session.setIsTokenActive(Boolean.FALSE);
//		sessionCachetRepo.save(session);
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setMessage("logged out successfully");
//		if (log.isDebugEnabled())
//			log.debug("Exiting inactivateSession service");
//		return cachet;
//	}
//}
