package com.spaceinje.gateway.serviceImpl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.spaceinje.gateway.bean.MailBean;
import com.spaceinje.gateway.bean.UserSessionBean;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.generator.EmailGenerator;
import com.spaceinje.gateway.generator.SecretKeyGenerator;
import com.spaceinje.gateway.model.LicenseDetails;
import com.spaceinje.gateway.model.UserInfo;
import com.spaceinje.gateway.model.UserSession;
import com.spaceinje.gateway.repository.UserInfoRepository;
import com.spaceinje.gateway.repository.UserSessionRepository;
import com.spaceinje.gateway.service.AuthService;
import com.spaceinje.gateway.specifications.CommonSpecs;
import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;
import com.spaceinje.gateway.validators.BasicValidator;

import brave.Tracer;

@Service("authService")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class AuthServiceImpl implements AuthService {

	private static final Logger log = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private GatewayConstants constants;

	@Autowired
	private SecretKeyGenerator secretKeyGenerator;

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Autowired
	private UserSessionRepository userSessionRepository;

	@Autowired
	private BasicValidator basicValidator;

	@Autowired
	private EmailGenerator emailGenerator;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private Tracer tracer;

	/**
	 * service to authorize the client or tenant
	 * 
	 * @param response_type - type of response required for the service
	 * @param client_id     - authorize a client based on valid client id
	 * @param tenant_secret - authorizing a tenant based on tenant secret
	 * @param scope         - scope of operations which client or tenant going to
	 *                      perform
	 * @return {@value ResponseCachet> contains the authorization status
	 */
	public ResponseCachet<UserSessionBean> authorize(String hash, String secret) {
		ResponseCachet<UserSessionBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone group with given shortcode");
			Optional<UserInfo> retrievedUser = userInfoRepository.findOne(CommonSpecs.findByHashCodeSpec(hash));
			if (!retrievedUser.isPresent()) {
				cachet.setMessage("No User found with provided details");
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug("No User found with provided details");
				return cachet;
			} else if (!retrievedUser.get().getSecret().equalsIgnoreCase(secret)) {
				cachet.setMessage("secret key doesn't match");
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug("secret key doesn't match");
				return cachet;
			} else if (!isTenantHasActiveLicenseInfo(retrievedUser.get())) {
				cachet.setMessage(retrievedUser.get().getTenantCode() + " has NO active license available");
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug(retrievedUser.get().getTenantCode() + " has NO active license available");
				return cachet;
			} else {
				UserSession userSession = new UserSession();
				String secretKey = secretKeyGenerator.generateRandomKey();
				String code = secretKeyGenerator.generateHash(constants.getStatus_response_type_code(), secretKey);
				userSession.setUserInfo(retrievedUser.get());
				userSession.setIsDeleted(Boolean.FALSE);
				userSession.setAuthCode(code);
				userSession.setAuthCodeExpiry(constants.getAuth_code_expiry_in_seconds());
				userSession.setIsAuthCodeActive(Boolean.TRUE);
				userSession.setAuthorizedAt(LocalDateTime.now(ZoneId.of("Asia/Kolkata")));
				userSession = userSessionRepository.save(userSession);
				log.debug("authorized successfully and created session object for the tenant : {}",
						retrievedUser.get().getTenantCode());
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("authorized the user successfully");
				cachet.setData(modelMapper.map(userSession, UserSessionBean.class));
				return cachet;
			}
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("Exception generating authcode while authorizing tenant: {}", e);
			cachet.setMessage("Exception while authorizing user");
			cachet.setStatus(constants.getStatus_message_exception());
			return cachet;
		} catch (Exception e) {
			log.error("Exception while authorizing tenant: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * method to check whether tenant has active license or not <br>
	 * <br>
	 * 
	 * <b>Condition</b>: active flag should be true and current date should be in
	 * between license start and end date
	 * 
	 * @param userInfo
	 * @return true if the mentioned conditions succeed, <br>
	 *         false otherwise
	 */
	private Boolean isTenantHasActiveLicenseInfo(UserInfo userInfo) {
		LicenseDetails licenseDetails = userInfo.getLicenseDetails();
		LocalDate currentDate = LocalDate.now(ZoneId.of("Asia/Kolkata"));
		return licenseDetails.getIsLicenseActive() && (!currentDate.isBefore(licenseDetails.getStartDate())
				&& !currentDate.isAfter(licenseDetails.getEndDate()));
	}

	/**
	 * service used for login
	 * 
	 * @param authCode - authorization code
	 * @param util - sign in uilitiy class
	 * 
	 * @return {@value ResponseCachet}
	 */
	@Override
	public ResponseCachet login(SigninUtil util, String authCode) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("fetch userInfo with username");

			Optional<UserInfo> retrievedUser = userInfoRepository
					.findOne(CommonSpecs.findByUserNameSpec(util.getUserName()));
			if (!retrievedUser.isPresent()) {
				cachet.setMessage("No User found with " + util.getUserName());
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug("No User found with " + util.getUserName());
				return cachet;
			} else if (!(retrievedUser.get().getLoginStatus().equalsIgnoreCase(constants.getLogin_status_activated())
					|| retrievedUser.get().getLoginStatus().equalsIgnoreCase(constants.getLogin_status_new()))) {
				cachet.setMessage(util.getUserName() + " unable to login");
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug(util.getUserName() + " unable to login");
				return cachet;
			} else if (retrievedUser.get().getLoginAttempts() >= Integer.valueOf(constants.getMax_login_attempts())) {
				cachet.setMessage("Account temporarily blocked, please reset your password or contact adminstrator");
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug("Account temporarily blocked, please reset your password or contact adminstrator");
				return cachet;
			} else if (!basicValidator.validateLoginPasswordMatch(util.getPassword(),
					retrievedUser.get().getPassword())) {
				UserInfo userInfo = retrievedUser.get();
				userInfo.setLoginAttempts(userInfo.getLoginAttempts() + 1);
				userInfoRepository.save(userInfo);
				cachet.setMessage("Invalid password, you have "
						+ (Integer.valueOf(constants.getMax_login_attempts()) - userInfo.getLoginAttempts())
						+ " attempts to login");
				cachet.setStatus(constants.getStatus_message_failure());
				log.debug("Invalid password, you have "
						+ (Integer.valueOf(constants.getMax_login_attempts()) - userInfo.getLoginAttempts())
						+ " attempts to login");
				return cachet;
			} else {

				UserInfo userInfo = retrievedUser.get();
				userInfo.setLoginAttempts(0);
				userInfoRepository.save(userInfo);

				Optional<UserSession> retrievedUserSession = userSessionRepository
						.findOne(CommonSpecs.findByAuthCodeSpec(authCode));

				if (!retrievedUserSession.isPresent()) {
					log.debug("invalid code, Please contact adminstrator");
					cachet.setMessage("invalid code, Please contact adminstrator");
					cachet.setStatus(constants.getStatus_message_failure());
					return cachet;
				} else {
					UserSession userSession = retrievedUserSession.get();
					if (!isSessionActive(userSession)) {
						log.debug("no active code or token available");
						cachet.setMessage("no active code or token available");
						cachet.setStatus(constants.getStatus_message_failure());
						userSessionRepository.save(userSession);
						return cachet;
					} else if (!userInfo.getTenantCode().equalsIgnoreCase(userSession.getUserInfo().getTenantCode())) {
						cachet.setMessage("Login failed. Please contact adminstrator");
						cachet.setStatus(constants.getStatus_message_failure());
						log.debug("Login for " + userInfo.getUserName()
								+ " failed. Please contact adminstrator, as the tenant code didnt matched");
						return cachet;
					}
					String secretKey = secretKeyGenerator.generateRandomKey();
					String authToken = secretKeyGenerator.generateHash(constants.getStatus_response_type_token(),
							secretKey);

					userSession.setUserInfo(userInfo);
					userSession.setAuthToken(authToken);
					userSession.setAuthTokenExpiry(constants.getAuth_token_expiry_in_seconds());
					userSession.setIsAuthTokenActive(Boolean.TRUE);
					userSession.setAuthenticatedAt(LocalDateTime.now(ZoneId.of("Asia/Kolkata")));
					userSession = userSessionRepository.save(userSession);
					log.debug("authenticated successfully and updated session object for the logged user : {}",
							retrievedUser.get().getUserName());
					cachet.setStatus(constants.getStatus_message_success());
					cachet.setMessage("authorized the user successfully");
					cachet.setData(modelMapper.map(userSession, UserSessionBean.class));
					return cachet;
				}
			}
		} catch (NumberFormatException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("Exception validating password or generating authToken while logging-in user: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("Exception validating password or generating authToken while logging-in user");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while logging-in user: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("Exception while logging-in user");
			return cachet;
		}
	}

	/**
	 * method to check whether session is active or not <br>
	 * <br>
	 * <b>Logic</b>: session's authCode should be active and current time should
	 * lies between authorizedAt and authorizedAt plus authCodeExpiry time, and <br>
	 * if authToken exists then authToken should be active and current time should
	 * lies between authenticatedAt and authenticatedAt plus authTokenExpiry time
	 * 
	 * @param userSession
	 * @return true if all the above conditions are success, <br>
	 *         false otherwise
	 */
	private Boolean isSessionActive(UserSession userSession) {
		return isAuthCodeActive(userSession) && isAuthTokenActive(userSession);
	}

	private Boolean isAuthCodeActive(UserSession userSession) {
		Boolean active = Boolean.TRUE;
		LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
		if (StringUtils.isNotBlank(userSession.getAuthCode())) {
			Boolean isAuthCodeActive = userSession.getIsAuthCodeActive()
					&& (!currentTime.isBefore(userSession.getAuthorizedAt()) && !currentTime
							.isAfter(userSession.getAuthorizedAt().plusSeconds(userSession.getAuthCodeExpiry())));
			if (!isAuthCodeActive) {
				userSession.setIsAuthCodeActive(isAuthCodeActive);
			}
			active = active && isAuthCodeActive;
		}
		log.debug("authCode active status : {}", active);
		return active;
	}

	private Boolean isAuthTokenActive(UserSession userSession) {
		Boolean active = Boolean.TRUE;
		LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
		if (StringUtils.isNotBlank(userSession.getAuthToken())) {
			Boolean isAuthTokenActive = userSession.getIsAuthTokenActive()
					&& (!currentTime.isBefore(userSession.getAuthenticatedAt()) && !currentTime
							.isAfter(userSession.getAuthenticatedAt().plusSeconds(userSession.getAuthTokenExpiry())));
			if (!isAuthTokenActive) {
				userSession.setIsAuthTokenActive(isAuthTokenActive);
			}
			active = active && isAuthTokenActive;
		}
		log.debug("authToken active status : {}", active);
		return active;
	}

	/**
	 * service used when tenant or admin forget their password
	 * 
	 * @param user_code - either email or mobile
	 * 
	 * @param is_tenant - to check whether service is accessed by tenant
	 * 
	 * @return {@value ResponseCachet}
	 */
	@Override
	public ResponseCachet forgotPassword(String userName, String authCode) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Optional<UserSession> retrievedUserSession = userSessionRepository
					.findOne(CommonSpecs.findByAuthCodeSpec(authCode));
			if (!retrievedUserSession.isPresent()) {
				log.debug("invalid code, Please contact adminstrator");
				cachet.setMessage("invalid code, Please contact adminstrator");
				cachet.setStatus(constants.getStatus_message_failure());
				return cachet;
			} else {
				UserSession userSession = retrievedUserSession.get();
				if (!isAuthCodeActive(userSession)) {
					log.debug("no active authcode available");
					cachet.setMessage("no active authcode available");
					cachet.setStatus(constants.getStatus_message_failure());
					userSessionRepository.save(userSession);
					return cachet;
				}
				Optional<UserInfo> retrievedUser = userInfoRepository.findOne(CommonSpecs.findByUserNameSpec(userName));
				if (!retrievedUser.isPresent()) {
					cachet.setMessage("No User found with " + userName);
					cachet.setStatus(constants.getStatus_message_failure());
					log.debug("No User found with " + userName);
					return cachet;
				} else if (!(retrievedUser.get().getLoginStatus()
						.equalsIgnoreCase(constants.getLogin_status_activated())
						|| retrievedUser.get().getLoginStatus().equalsIgnoreCase(constants.getLogin_status_new()))) {
					cachet.setMessage(userName + " unable to login");
					cachet.setStatus(constants.getStatus_message_failure());
					log.debug(userName + " unable to login");
					return cachet;
				} else if (!retrievedUser.get().getTenantCode()
						.equalsIgnoreCase(userSession.getUserInfo().getTenantCode())) {
					cachet.setMessage("Reset password failed. Please contact adminstrator");
					cachet.setStatus(constants.getStatus_message_failure());
					log.debug("Reset password " + userName
							+ " failed. Please contact adminstrator, as the tenant code didnt matched");
					return cachet;
				}
				UserInfo userInfo = retrievedUser.get();
				String temporaryPassword = secretKeyGenerator.generatePassword() + constants.getPassword_suffix();
				userInfo.setPassword(secretKeyGenerator.generateHash(constants.getStatus_response_type_password(),
						temporaryPassword));
				userInfo.setLoginStatus(constants.getLogin_status_new());
				userInfo = userInfoRepository.save(userInfo);
				log.debug("password reset successful");

				forgotPasswordMailSender(userInfo, temporaryPassword);
				log.debug("email sent successfully for password reset");

				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("password reset successful and shared via email");
				return cachet;
			}
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("exception while generating new password : {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("exception while generating new password");
			return cachet;
		} catch (Exception e) {
			log.error("exception while resetting password : {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("exception while resetting password");
			return cachet;
		}
	}

	private ResponseCachet<?> forgotPasswordMailSender(UserInfo user, String passcode) {
		ResponseCachet<?> response = new ResponseCachet<>();
		try {
			log.debug("preparing the mail bean props to send forgot password mail to user");
			MailBean mail = new MailBean();
			mail.setSubject(constants.getForgot_password_subject());
			mail.setTemplate_name("forgot_password.html");
			mail.setHas_attachement(Boolean.FALSE);
			mail.setTo(user.getEmail());
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("Username", user.getFirstName());
			model.put("password", passcode);
			mail.setModel(model);

			log.debug("calling email generator service");
			return emailGenerator.sendMail(mail);
		} catch (Exception e) {
			log.error("Exception sending forgot pasword mail: {}", e);
			response.setMessage("Exception sending forgot pasword mail");
			response.setStatus(constants.getStatus_message_failure());
			return response;
		}

	}

	/**
	 * method to reset the password
	 */
	@Override
	public ResponseCachet resetPassword(SigninUtil util, String authToken) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Optional<UserSession> retrievedUserSession = userSessionRepository
					.findOne(CommonSpecs.findByAuthTokenSpec(authToken));
			if (!retrievedUserSession.isPresent()) {
				log.debug("Invalid token, there is no session associated to the given token");
				cachet.setMessage("Invalid token, there is no session associated to the given token");
				cachet.setStatus(constants.getStatus_message_failure());
				return cachet;
			} else {
				UserSession userSession = retrievedUserSession.get();
				if (!isSessionActive(userSession)) {
					log.debug("no active session available");
					cachet.setMessage("no active session available");
					cachet.setStatus(constants.getStatus_message_failure());
					userSessionRepository.save(userSession);
					return cachet;
				}
				Optional<UserInfo> retrievedUser = userInfoRepository
						.findOne(CommonSpecs.findByUserNameSpec(util.getUserName()));
				if (!retrievedUser.isPresent()) {
					cachet.setMessage("No User found with " + util.getUserName());
					cachet.setStatus(constants.getStatus_message_failure());
					log.debug("No User found with " + util.getUserName());
					return cachet;
				} else {
					UserInfo userInfo = retrievedUser.get();
//					if (!(userInfo.getLoginStatus().equalsIgnoreCase(constants.getLogin_status_activated())
//							|| userInfo.getLoginStatus().equalsIgnoreCase(constants.getLogin_status_new()))) {
//						cachet.setMessage(util.getUserName() + " unable to access, due to login status issue");
//						cachet.setStatus(constants.getStatus_message_failure());
//						log.debug(util.getUserName() + " unable to login, due to login status issue");
//						return cachet;
//					}
					if (!userInfo.getTenantCode().equalsIgnoreCase(userSession.getUserInfo().getTenantCode())) {
						cachet.setMessage("Invalid token for the user, Please contact adminstrator");
						cachet.setStatus(constants.getStatus_message_failure());
						log.debug("Invalid token for the user : " + util.getUserName()
								+ ". Please contact adminstrator, as the tenant code didnt matched");
						return cachet;
					} else if (!basicValidator.validateLoginPasswordMatch(util.getPassword(), userInfo.getPassword())) {
						userInfo.setLoginAttempts(userInfo.getLoginAttempts() + 1);
						userInfoRepository.save(userInfo);
						cachet.setMessage("Invalid password, you have "
								+ (Integer.valueOf(constants.getMax_login_attempts()) - userInfo.getLoginAttempts())
								+ " attempts to login");
						cachet.setStatus(constants.getStatus_message_failure());
						log.debug("Invalid password, you have "
								+ (Integer.valueOf(constants.getMax_login_attempts()) - userInfo.getLoginAttempts())
								+ " attempts to login");
						return cachet;
					} else if (!basicValidator.isPasswordValid(util.getNewPassword())) {
						cachet.setMessage(
								"Invalid Password : It contains at least 8 characters and at most 20 characters.\r\n"
										+ "     *           It contains at least one digit.\r\n"
										+ "     *           It contains at least one upper case alphabet.\r\n"
										+ "     *           It contains at least one lower case alphabet.\r\n"
										+ "     *           It contains at least one special character which includes !@#$%&*()-+=^.\r\n"
										+ "     *           It does not contain any white space.");
						cachet.setStatus(constants.getStatus_message_failure());
						log.debug("Invalid Password");
						return cachet;
					}
					userInfo.setPassword(secretKeyGenerator.generateHash(constants.getStatus_response_type_password(),
							util.getNewPassword()));
					userInfo.setLoginStatus(constants.getLogin_status_activated());
					userInfoRepository.save(userInfo);
					log.debug("password reset successful");
					cachet.setStatus(constants.getStatus_message_success());
					cachet.setMessage("Password reset successful, Please login to continue.!");
					return cachet;
				}
			}
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("exception while generating new password : {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("exception while generating new password");
			return cachet;
		} catch (Exception e) {
			log.error("exception while resetting password : {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("exception while resetting password");
			return cachet;
		}
	}

	@Caching(put = { @CachePut(value = "RESPONSE_CACHE", key = "#authToken") })
	@Override
	public ResponseCachet findSessionByToken(String authToken) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		Optional<UserSession> retrievedUserSession = userSessionRepository
				.findOne(CommonSpecs.findByAuthTokenSpec(authToken));
		if (!retrievedUserSession.isPresent()) {
			log.debug("Invalid token, Please relogin to get the latest token");
			cachet.setMessage("Invalid token, Please relogin to get the latest token");
			cachet.setStatus(constants.getStatus_message_failure());
			return cachet;
		} else {
			UserSession userSession = retrievedUserSession.get();
			if (!isSessionActive(userSession)) {
				log.debug("no active session available");
				cachet.setMessage("no active session available");
				cachet.setStatus(constants.getStatus_message_failure());
				userSessionRepository.save(userSession);
				return cachet;
			}
//			String userName = "loggedin user";
//			// TODO: find out a way to get the username and 
//			// check whether the token is of logged-in user's only or not
//			Optional<UserInfo> retrievedUser = userInfoRepository.findOne(CommonSpecs.findByUserNameSpec(userName));
//			if (!retrievedUser.isPresent()) {
//				cachet.setMessage("No User found with " + userName);
//				cachet.setStatus(constants.getStatus_message_failure());
//				log.debug("No User found with " + userName);
//				return cachet;
//			} else if (!(retrievedUser.get().getLoginStatus()
//					.equalsIgnoreCase(constants.getLogin_status_activated())
//					|| retrievedUser.get().getLoginStatus().equalsIgnoreCase(constants.getLogin_status_new()))) {
//				cachet.setMessage(userName + " unable to access, due to login status issue");
//				cachet.setStatus(constants.getStatus_message_failure());
//				log.debug(userName + " unable to login, due to login status issue");
//				return cachet;
//			} else if (!retrievedUser.get().getTenantCode()
//					.equalsIgnoreCase(userSession.getUserInfo().getTenantCode())) {
//				cachet.setMessage("Invalid token for the user, Please contact adminstrator");
//				cachet.setStatus(constants.getStatus_message_failure());
//				log.debug("Invalid token for the user : " + userName
//						+ ". Please contact adminstrator, as the tenant code didnt matched");
//				return cachet;
//			}
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved active session");
			cachet.setData(modelMapper.map(userSession, UserSessionBean.class));
			return cachet;
		}
	}

}
