/**
 * 
 */
package com.spaceinje.gateway.repo.nosql;

import java.util.List;

import com.spaceinje.gateway.model.nosql.SessionTracker;

/**
 * @author Lakshmi Kiran
 * @implNote repository to interact with session tracker collection in mongo
 * @version 1.0
 */
public interface SessionTrackerRepo {

	SessionTracker findByCode(String auth_code);
	
	SessionTracker findByHash(String hash);

	SessionTracker findByAuthToken(String auth_token);

	List<SessionTracker> getInActiveCodeSessions(boolean status);

	List<SessionTracker> getInActiveTokenSessions(boolean status);

	SessionTracker save(SessionTracker session);
	
	
}
