/**
 * 
 */
package com.spaceinje.gateway.repo.nosql.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.spaceinje.gateway.model.nosql.UserInfo;
import com.spaceinje.gateway.repo.nosql.UserInfoRepo;


/**
 * @author Lakshmi Kiran
 *
 */
@Repository
public class UserInfoRepoImpl implements UserInfoRepo {

	private final MongoTemplate mongoTemplate;
	
    @Autowired
    public UserInfoRepoImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
	
	@Override
	public UserInfo findByHash(String hash) {
		Query query = new Query();
		query.addCriteria(Criteria.where("hash").is(hash));
		return mongoTemplate.findOne(query, UserInfo.class);
	}

	@Override
	public UserInfo findByEmailOrMobile(String email, String mobile, boolean is_tenant) {
		Query query = new Query();
		query.addCriteria(Criteria.where("email").is(email).orOperator(Criteria.where("mobile").is(mobile))
				.andOperator(Criteria.where("is_tenant").is(is_tenant)));
		return mongoTemplate.findOne(query, UserInfo.class);
	}

	@Override
	public UserInfo save(UserInfo info) {
		return mongoTemplate.save(info);
	}

	@Override
	public UserInfo findById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, UserInfo.class);
	}

	@Override
	public void delete(UserInfo info) {
		mongoTemplate.remove(info);
	}

	@Override
	public UserInfo findByAdminCode(String admin_code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("admin_code").is(admin_code));
		return mongoTemplate.findOne(query, UserInfo.class);
	}

	@Override
	public UserInfo findByTenantCode(String tenant_code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("tenant_code").is(tenant_code));
		return mongoTemplate.findOne(query, UserInfo.class);
	}

}
