/**
 * 
 */
package com.spaceinje.gateway.repo.nosql;

import com.spaceinje.gateway.model.nosql.UserInfo;

/**
 * @author Lakshmi Kiran
 *
 */
public interface UserInfoRepo {

	UserInfo findByHash(String hash);

	public UserInfo findByEmailOrMobile(String email, String mobile, boolean is_tenant);

	public UserInfo save(UserInfo info);

	public UserInfo findById(String id);

	public void delete(UserInfo info);

	public UserInfo findByAdminCode(String admin_code);

	public UserInfo findByTenantCode(String tenant_code);
}
