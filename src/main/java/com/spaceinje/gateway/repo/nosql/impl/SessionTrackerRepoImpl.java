/**
 * 
 */
package com.spaceinje.gateway.repo.nosql.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.spaceinje.gateway.model.nosql.SessionTracker;
import com.spaceinje.gateway.repo.nosql.SessionTrackerRepo;

/**
 * @author Lakshmi Kiran
 *
 */
@Repository
public class SessionTrackerRepoImpl implements SessionTrackerRepo {

private final MongoTemplate mongoTemplate;
	
    @Autowired
    public SessionTrackerRepoImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
	
	@Override
	public SessionTracker findByCode(String auth_code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("auth_code").is(auth_code));
		query.addCriteria(Criteria.where("is_code_active").is(true));
		return mongoTemplate.findOne(query, SessionTracker.class);
	}

	@Override
	public SessionTracker findByAuthToken(String auth_token) {
		Query query = new Query();
		query.addCriteria(Criteria.where("auth_token").is(auth_token));
		query.addCriteria(Criteria.where("is_token_active").is(true));
		return mongoTemplate.findOne(query, SessionTracker.class);
	}

	@Override
	public List<SessionTracker> getInActiveCodeSessions(boolean status) {
		Query query = new Query();
		query.addCriteria(Criteria.where("is_code_active").is(status));
		return mongoTemplate.find(query, SessionTracker.class);
	}

	@Override
	public List<SessionTracker> getInActiveTokenSessions(boolean status) {
		Query query = new Query();
		query.addCriteria(Criteria.where("is_token_active").is(status));
		return mongoTemplate.find(query, SessionTracker.class);
	}

	@Override
	public SessionTracker save(SessionTracker session) {
		return mongoTemplate.save(session);		
	}

	@Override
	public SessionTracker findByHash(String hash) {
		Query query = new Query();
		query.addCriteria(Criteria.where("hash").is(hash));
		query.addCriteria(Criteria.where("is_code_active").is(true));
		return mongoTemplate.findOne(query, SessionTracker.class);
	}

}
