/**
 * 
 */
package com.spaceinje.gateway.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.spaceinje.gateway.bean.MailBean;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.util.ResponseCachet;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author Lakshmi Kiran
 * @implNote class which send the email to users upon registration
 * @version 1.0
 */
@Service("emailGenerator")
public class EmailGenerator {

	private static final Logger log = LoggerFactory.getLogger(EmailGenerator.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private Configuration configuration;

	@Autowired
	private GatewayConstants constants;

	/**
	 * method to send email to Admin upon registration
	 * 
	 * @param mail - MailBean
	 * 
	 * @return {@value ResponseCachet}
	 */
	@SuppressWarnings("rawtypes")
	public ResponseCachet sendMail(MailBean mail) {
		if (log.isDebugEnabled())
			log.debug("Entering sendMail service");
		ResponseCachet cachet = new ResponseCachet();
		try {
			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true, CharEncoding.UTF_8);

			configuration.setClassForTemplateLoading(this.getClass(), "\\templates");
			Template template = configuration.getTemplate(mail.getTemplate_name());
			String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, mail.getModel());
			helper.setTo(new InternetAddress(mail.getTo()));
			helper.setText(text, true);
			helper.setSubject(mail.getSubject());
			helper.setSentDate(new Date());
			helper.setFrom(constants.getFrom_email());
			if (mail.isHas_attachement()) {
				InputStream inputStream = new FileInputStream(new File(mail.getFilepath()));
				helper.addAttachment(MimeUtility.encodeText(mail.getFile_name()),
						new ByteArrayResource(IOUtils.toByteArray(inputStream)), mail.getMedia_type());

				log.info("closig the input stream");
				inputStream.close();
			}
			javaMailSender.send(message);
			log.info("mail sent successfilly");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("mail sent successfilly");
			if (log.isDebugEnabled())
				log.debug("Exiting sendMail service");
			return cachet;
		} catch (IOException e) {
			log.error("IOException :: file not found", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			if (log.isDebugEnabled())
				log.debug("Exiting sendMail service");
			return cachet;
		} catch (TemplateException e) {
			log.error("TemplateException :: template not found", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			if (log.isDebugEnabled())
				log.debug("Exiting sendMail service");
			return cachet;
		} catch (AddressException e) {
			log.error("AddressException :: email not found", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			if (log.isDebugEnabled())
				log.debug("Exiting sendMail service");
			return cachet;
		} catch (MessagingException e) {
			log.error("MessagingException :: invalid mail message", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			if (log.isDebugEnabled())
				log.debug("Exiting sendMail service");
			return cachet;
		}
	}
}
