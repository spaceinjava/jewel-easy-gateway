/**
 * 
 */
package com.spaceinje.gateway.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.spaceinje.gateway.constants.GatewayConstants;

/**
 * @author Lakshmi Kiran
 * @implNote class which generates user secret code and secret as pdf file
 * @version 1.0 {@value pdf file}
 */
@Component
public class PDFGenerator {

	private static final Logger log = LoggerFactory.getLogger(PDFGenerator.class);

	@Autowired
	private GatewayConstants constants;

	/**
	 * create a pdf file with users hash and secret
	 * 
	 * @param hash     - user hash
	 * @param secret   - user secret
	 * @param username - first name of the user
	 * @param user_id  - id of the user
	 * @param filePass - password for the file
	 * @return - path of generated pdf
	 * @throws DocumentException
	 * @throws IOException
	 */
	public String createPDF(String hash, String secret, String username, String user_id, String filePass)
			throws DocumentException, IOException {
		if (log.isDebugEnabled())
			log.debug("Entering create pdf service");
		PdfReader reader = null;
		PdfStamper stamper = null;
		log.info("creating document");
		Document document = new Document();
		log.info("creating pdf file");
		ClassLoader classLoader = getClass().getClassLoader();
		if (constants.getEnv_type().equals("local")) {
			File file = new File(classLoader.getResource(".").getFile() + username + user_id + ".pdf");
			file.createNewFile();
			PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
		} else {
			log.info("creating file at : "+constants.getPdf_location()+" for "+constants.getEnv_type()+" env");
			PdfWriter.getInstance(document, new FileOutputStream(constants.getPdf_location() + username + user_id + ".pdf"));
		}
		log.info("opening the document to write the content");
		document.open();
		log.info("adding image to document");
		Image image = Image.getInstance("classpath:files/logo.jpeg");
		image.scaleToFit(PageSize.POSTCARD.getWidth(), PageSize.POSTCARD.getHeight());
		image.setBorderColor(BaseColor.RED);
		document.add(image);

		Font f1 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		f1.setSize(18);
		f1.setColor(BaseColor.GRAY);

		log.info("adding a paragraph of spaces");
		Paragraph p4 = new Paragraph("                                               ", f1);

		document.add(p4);
		document.add(p4);

		log.info("adding header paragraph");
		Paragraph p = new Paragraph("User Details", f1);
		p.setAlignment(Paragraph.ALIGN_CENTER);

		log.info("adding more details for the document");
		document.addAuthor("SpaceInJe");
		document.addCreationDate();
		document.addSubject("secret details which will be used by users");
		document.addTitle("Jewel Easy");
		document.bottom();
		document.top();
		document.left();
		document.right();
		document.add(p);

		log.info("adding cells to documents");
		PdfPTable table = new PdfPTable(2); // 2 columns.
		table.setWidthPercentage(80); // Width 80%
		table.setSpacingBefore(10f); // Space before table
		table.setSpacingAfter(10f); // Space after table
		float[] columnWidths = { 10f, 10f };
		table.setWidths(columnWidths);

		PdfPCell cell1 = new PdfPCell(new Paragraph("hash"));
		cell1.setBorderColor(BaseColor.BLACK);
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell c1 = new PdfPCell(new Paragraph(hash));

		PdfPCell cell2 = new PdfPCell(new Paragraph("secret"));
		cell2.setBorderColor(BaseColor.BLACK);
		cell2.setPaddingLeft(10);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell c2 = new PdfPCell(new Paragraph(secret));

		table.addCell(cell1);
		table.addCell(c1);
		table.addCell(cell2);
		table.addCell(c2);

		log.info("adding table to document");
		document.add(table);
		log.info("closing the document");
		document.close();
		String path = null;
		log.info("securing the pdf file with filepass");
		if (constants.getEnv_type().equals("local")) {
			File file = new File(classLoader.getResource(".").getFile() + username + user_id + ".pdf");
			reader = new PdfReader(file.getAbsolutePath());
			File enc_file = new File(classLoader.getResource(".").getFile() + "JE_" + username + user_id + ".pdf");
			enc_file.createNewFile();
			stamper = new PdfStamper(reader, new FileOutputStream(enc_file.getAbsoluteFile()));
			stamper.setEncryption(filePass.getBytes(), filePass.getBytes(), 0, PdfWriter.ENCRYPTION_AES_256);
			path = enc_file.getAbsolutePath();
		} else {
			reader = new PdfReader(constants.getPdf_location() + username + user_id + ".pdf");
			stamper = new PdfStamper(reader,
					new FileOutputStream(constants.getPdf_location() + "JE_" + username + user_id + ".pdf"));
			stamper.setEncryption(filePass.getBytes(), filePass.getBytes(), 0, PdfWriter.ENCRYPTION_AES_256);
			path = constants.getPdf_location() + "JE_" + username + user_id + ".pdf";
		}
		stamper.close();
		reader.close();
		if (log.isDebugEnabled())
			log.debug("Exiting create pdf service");
		return path;
	}
}
