/**
 * 
 */
package com.spaceinje.gateway.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

//@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionTracker extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 7724432378951389200L;

	@JsonInclude(Include.NON_NULL)
	private Boolean isTenant;

	@JsonInclude(Include.NON_NULL)
	private String hash;

	@JsonInclude(Include.NON_NULL)
	private String authCode;

	@JsonInclude(Include.NON_NULL)
	private String authToken;
	
	@JsonInclude(Include.NON_NULL)
	private int codeExpiry;
	
	@JsonInclude(Include.NON_NULL)
	private int tokenExpiry;

	@JsonInclude(Include.NON_NULL)
	private boolean isCodeActive;
	
	@JsonInclude(Include.NON_NULL)
	private boolean isTokenActive;

	@JsonInclude(Include.NON_NULL)
	private Date authorizedDate;
	
	@JsonInclude(Include.NON_NULL)
	private Date authenticatedDate;

	public Boolean getIsTenant() {
		return isTenant;
	}

	public void setIsTenant(Boolean isTenant) {
		this.isTenant = isTenant;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public int getCodeExpiry() {
		return codeExpiry;
	}

	public void setCodeExpiry(int codeExpiry) {
		this.codeExpiry = codeExpiry;
	}

	public int getTokenExpiry() {
		return tokenExpiry;
	}

	public void setTokenExpiry(int tokenExpiry) {
		this.tokenExpiry = tokenExpiry;
	}

	public boolean getIsCodeActive() {
		return isCodeActive;
	}

	public void setIsCodeActive(boolean isCodeActive) {
		this.isCodeActive = isCodeActive;
	}

	public boolean getIsTokenActive() {
		return isTokenActive;
	}

	public void setIsTokenActive(boolean isTokenActive) {
		this.isTokenActive = isTokenActive;
	}

	public Date getAuthorizedDate() {
		return authorizedDate;
	}

	public void setAuthorizedDate(Date authorizedDate) {
		this.authorizedDate = authorizedDate;
	}

	public Date getAuthenticatedDate() {
		return authenticatedDate;
	}

	public void setAuthenticatedDate(Date authenticatedDate) {
		this.authenticatedDate = authenticatedDate;
	}

	@Override
	public String toString() {
		return "SessionTracker [isTenant=" + isTenant + ", hash=" + hash + ", authCode=" + authCode + ", authToken="
				+ authToken + ", codeExpiry=" + codeExpiry + ", tokenExpiry=" + tokenExpiry + ", isCodeActive="
				+ isCodeActive + ", isTokenActive=" + isTokenActive + ", authorizedDate=" + authorizedDate
				+ ", authenticatedDate=" + authenticatedDate + "]";
	}

	
	
}
