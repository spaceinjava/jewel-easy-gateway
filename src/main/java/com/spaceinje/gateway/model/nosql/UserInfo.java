/**
 * 
 */
package com.spaceinje.gateway.model.nosql;

import java.io.Serializable;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Lakshmi Kiran
 * @implNote Back end admin or super admin who can perform operations on tenant,
 *           master and inventory transactions
 * @version 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo extends AuditEntity implements Serializable {

	/**
	 * {@value serialVersionUID}
	 */
	private static final long serialVersionUID = 4113432245845931625L;

	@BsonProperty("_id")
	@Id
	private String id;
	
	@JsonInclude(Include.NON_NULL)
	private boolean is_tenant;
	
	@JsonInclude(Include.NON_NULL)
	private String admin_code;
	
	@JsonInclude(Include.NON_NULL)
	private String tenant_code;
	
	@JsonInclude(Include.NON_NULL)
	private String emp_code;

	@JsonInclude(Include.NON_NULL)
	private String first_name;

	@JsonInclude(Include.NON_NULL)
	private String middle_name;

	@JsonInclude(Include.NON_NULL)
	private String last_name;

	@JsonInclude(Include.NON_NULL)
	private String email;

	@JsonInclude(Include.NON_NULL)
	private String mobile;

	@JsonInclude(Include.NON_NULL)
	private String password;
	
	@JsonInclude(Include.NON_NULL)
	private String profile_picture;

	@JsonInclude(Include.NON_NULL)
	private Integer login_attempts;
	
	@JsonInclude(Include.NON_NULL)
	private String login_status;
	
	@JsonInclude(Include.NON_NULL)
	private String hash;

	@JsonInclude(Include.NON_NULL)
	private String secret;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the is_tenant
	 */
	public boolean getIs_tenant() {
		return is_tenant;
	}

	/**
	 * @param is_tenant the is_tenant to set
	 */
	public void setIs_tenant(boolean is_tenant) {
		this.is_tenant = is_tenant;
	}

	/**
	 * @return the admin_id
	 */
	public String getAdmin_code() {
		return admin_code;
	}

	/**
	 * @param admin_id the admin_id to set
	 */
	public void setAdmin_code(String admin_id) {
		this.admin_code = admin_id;
	}

	/**
	 * @return the tenant_id
	 */
	public String getTenant_code() {
		return tenant_code;
	}

	/**
	 * @param tenant_id the tenant_id to set
	 */
	public void setTenant_code(String tenant_id) {
		this.tenant_code = tenant_id;
	}

	/**
	 * @return the emp_code
	 */
	public String getEmp_code() {
		return emp_code;
	}

	/**
	 * @param emp_code the emp_code to set
	 */
	public void setEmp_code(String emp_code) {
		this.emp_code = emp_code;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the middle_name
	 */
	public String getMiddle_name() {
		return middle_name;
	}

	/**
	 * @param middle_name the middle_name to set
	 */
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the login_attempts
	 */
	public Integer getLogin_attempts() {
		return login_attempts;
	}

	/**
	 * @param login_attempts the login_attempts to set
	 */
	public void setLogin_attempts(Integer login_attempts) {
		this.login_attempts = login_attempts;
	}

	/**
	 * @return the login_status
	 */
	public String getLogin_status() {
		return login_status;
	}

	/**
	 * @param login_status the login_status to set
	 */
	public void setLogin_status(String login_status) {
		this.login_status = login_status;
	}

	/**
	 * @return the profile_picture
	 */
	public String getProfile_picture() {
		return profile_picture;
	}

	/**
	 * @param profile_picture the profile_picture to set
	 */
	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * @param secret the secret to set
	 */
	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", is_tenant=" + is_tenant + ", admin_code=" + admin_code + ", tenant_code="
				+ tenant_code + ", emp_code=" + emp_code + ", first_name=" + first_name + ", middle_name=" + middle_name
				+ ", last_name=" + last_name + ", email=" + email + ", mobile=" + mobile + ", password=" + password
				+ ", profile_picture=" + profile_picture + ", login_attempts=" + login_attempts + ", login_status="
				+ login_status + ", hash=" + hash + ", secret=" + secret + "]";
	}
}
