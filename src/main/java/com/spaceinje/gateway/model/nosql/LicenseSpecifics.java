/**
 * 
 */
package com.spaceinje.gateway.model.nosql;

import java.io.Serializable;
import java.util.Date;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Lakshmi Kiran
 * @implNote class which contains license details of the tenant
 * @version 2.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LicenseSpecifics implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8100387641789194176L;

	@BsonProperty("_id")
	@Id
	private String id;
	
	@JsonInclude(Include.NON_NULL)
	private String tenant_code;

	@JsonInclude(Include.NON_NULL)
	private Boolean is_tenant_license_free;
	
	@JsonInclude(Include.NON_NULL)
	private String license_type;

	@JsonInclude(Include.NON_NULL)
	private int license_active_days;

	@JsonInclude(Include.NON_NULL)
	@DateTimeFormat
	private Date license_start_date;

	@JsonInclude(Include.NON_NULL)
	@DateTimeFormat
	private Date license_end_date;

	@JsonInclude(Include.NON_NULL)
	private Boolean is_tenant_license_active;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTenant_code() {
		return tenant_code;
	}

	public void setTenant_code(String tenant_code) {
		this.tenant_code = tenant_code;
	}

	public Boolean getIs_tenant_license_free() {
		return is_tenant_license_free;
	}

	public void setIs_tenant_license_free(Boolean is_tenant_license_free) {
		this.is_tenant_license_free = is_tenant_license_free;
	}

	public String getLicense_type() {
		return license_type;
	}

	public void setLicense_type(String license_type) {
		this.license_type = license_type;
	}

	public int getLicense_active_days() {
		return license_active_days;
	}

	public void setLicense_active_days(int license_active_days) {
		this.license_active_days = license_active_days;
	}

	public Date getLicense_start_date() {
		return license_start_date;
	}

	public void setLicense_start_date(Date license_start_date) {
		this.license_start_date = license_start_date;
	}

	public Date getLicense_end_date() {
		return license_end_date;
	}

	public void setLicense_end_date(Date license_end_date) {
		this.license_end_date = license_end_date;
	}

	public Boolean getIs_tenant_license_active() {
		return is_tenant_license_active;
	}

	public void setIs_tenant_license_active(Boolean is_tenant_license_active) {
		this.is_tenant_license_active = is_tenant_license_active;
	}

	@Override
	public String toString() {
		return "LicenseSpecifics [id=" + id + ", tenant_code=" + tenant_code + ", is_tenant_license_free="
				+ is_tenant_license_free + ", license_type=" + license_type + ", license_active_days="
				+ license_active_days + ", license_start_date=" + license_start_date + ", license_end_date="
				+ license_end_date + ", is_tenant_license_active=" + is_tenant_license_active + "]";
	}

}
