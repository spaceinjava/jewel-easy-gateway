/**
 * 
 */
package com.spaceinje.gateway.model.nosql;

import java.io.Serializable;
import java.util.Date;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Lakshmi Kiran
 * 
 * @implNote class which stores complete information of user sessions along with
 *           the authorized and accessed services.
 * 
 * @category session tracking
 * 
 * @version 1.0
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionTracker implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7724432378951389200L;

	@BsonProperty("_id")
	@Id
	private String id;

	@JsonInclude(Include.NON_NULL)
	private Boolean is_tenant;

	@JsonInclude(Include.NON_NULL)
	private String hash;

	@JsonInclude(Include.NON_NULL)
	private String auth_code;

	@JsonInclude(Include.NON_NULL)
	private String auth_token;
	
	@JsonInclude(Include.NON_NULL)
	private int code_expiry;
	
	@JsonInclude(Include.NON_NULL)
	private int token_expiry;

	@JsonInclude(Include.NON_NULL)
	private boolean is_code_active;
	
	@JsonInclude(Include.NON_NULL)
	private boolean is_token_active;

	@JsonInclude(Include.NON_NULL)
	private Date authorized_date;
	
	@JsonInclude(Include.NON_NULL)
	private Date authenticated_date;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the is_tenant
	 */
	public Boolean getIs_tenant() {
		return is_tenant;
	}

	/**
	 * @param is_tenant the is_tenant to set
	 */
	public void setIs_tenant(Boolean is_tenant) {
		this.is_tenant = is_tenant;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the auth_code
	 */
	public String getAuth_code() {
		return auth_code;
	}

	/**
	 * @param auth_code the auth_code to set
	 */
	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}

	/**
	 * @return the auth_token
	 */
	public String getAuth_token() {
		return auth_token;
	}

	/**
	 * @param auth_token the auth_token to set
	 */
	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}

	/**
	 * @return the code_expiry
	 */
	public int getCode_expiry() {
		return code_expiry;
	}

	/**
	 * @param code_expiry the code_expiry to set
	 */
	public void setCode_expiry(int code_expiry) {
		this.code_expiry = code_expiry;
	}

	/**
	 * @return the token_expiry
	 */
	public int getToken_expiry() {
		return token_expiry;
	}

	/**
	 * @param token_expiry the token_expiry to set
	 */
	public void setToken_expiry(int token_expiry) {
		this.token_expiry = token_expiry;
	}

	/**
	 * @return the is_code_active
	 */
	public boolean isIs_code_active() {
		return is_code_active;
	}

	/**
	 * @param is_code_active the is_code_active to set
	 */
	public void setIs_code_active(boolean is_code_active) {
		this.is_code_active = is_code_active;
	}

	/**
	 * @return the is_token_active
	 */
	public boolean isIs_token_active() {
		return is_token_active;
	}

	/**
	 * @param is_token_active the is_token_active to set
	 */
	public void setIs_token_active(boolean is_token_active) {
		this.is_token_active = is_token_active;
	}

	/**
	 * @return the authorized_date
	 */
	public Date getAuthorized_date() {
		return authorized_date;
	}

	/**
	 * @param authorized_date the authorized_date to set
	 */
	public void setAuthorized_date(Date authorized_date) {
		this.authorized_date = authorized_date;
	}

	/**
	 * @return the authenticated_date
	 */
	public Date getAuthenticated_date() {
		return authenticated_date;
	}

	/**
	 * @param authenticated_date the authenticated_date to set
	 */
	public void setAuthenticated_date(Date authenticated_date) {
		this.authenticated_date = authenticated_date;
	}

	@Override
	public String toString() {
		return "SessionTracker [id=" + id + ", is_tenant=" + is_tenant + ", hash=" + hash + ", auth_code=" + auth_code
				+ ", auth_token=" + auth_token + ", code_expiry=" + code_expiry + ", token_expiry=" + token_expiry
				+ ", is_code_active=" + is_code_active + ", is_token_active=" + is_token_active + ", authorized_date="
				+ authorized_date + ", authenticated_date=" + authenticated_date + "]";
	}

	
}
