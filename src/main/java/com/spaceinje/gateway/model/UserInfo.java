package com.spaceinje.gateway.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class UserInfo extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 4113432245845931625L;

	private Boolean isTenant;

//	private String admin_code;

//	private String emp_code;

	private String firstName;

	private String middleName;

	private String lastName;

	private String email;

	private String mobile;

	private String userName;

	private String password;

//	private String profile_picture;

	private Integer loginAttempts;

	private String loginStatus;

	private String hash;

	private String secret;

	@OneToOne(mappedBy = "userInfo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private LicenseDetails licenseDetails;

	@OneToMany(mappedBy = "userInfo", fetch = FetchType.LAZY)
	private Set<UserSession> userSessions;

	public Boolean getIsTenant() {
		return isTenant;
	}

	public void setIsTenant(Boolean isTenant) {
		this.isTenant = isTenant;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Integer loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public LicenseDetails getLicenseDetails() {
		return licenseDetails;
	}

	public void setLicenseDetails(LicenseDetails licenseDetails) {
		this.licenseDetails = licenseDetails;
	}

	public Set<UserSession> getUserSessions() {
		return userSessions;
	}

	public void setUserSessions(Set<UserSession> userSessions) {
		this.userSessions = userSessions;
	}

	@Override
	public String toString() {
		return "UserInfo [isTenant=" + isTenant + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", email=" + email + ", mobile=" + mobile + ", userName=" + userName
				+ ", password=" + password + ", loginAttempts=" + loginAttempts + ", loginStatus=" + loginStatus
				+ ", hash=" + hash + ", secret=" + secret + ", licenseDetails=" + licenseDetails + ", userSessions="
				+ userSessions + "]";
	}

}
