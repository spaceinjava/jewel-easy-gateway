package com.spaceinje.gateway.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class LicenseDetails extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -7593932144179858464L;

	private Boolean isLicenseFree;

	private String licenseType;

	@DateTimeFormat
	private LocalDate startDate;

	@DateTimeFormat
	private LocalDate endDate;

	private Boolean isLicenseActive;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userInfoId")
	private UserInfo userInfo;

	public Boolean getIsLicenseFree() {
		return isLicenseFree;
	}

	public void setIsLicenseFree(Boolean isLicenseFree) {
		this.isLicenseFree = isLicenseFree;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsLicenseActive() {
		return isLicenseActive;
	}

	public void setIsLicenseActive(Boolean isLicenseActive) {
		this.isLicenseActive = isLicenseActive;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public String toString() {
		return "LicenseDetails [isLicenseFree=" + isLicenseFree + ", licenseType=" + licenseType + ", startDate="
				+ startDate + ", endDate=" + endDate + ", isLicenseActive=" + isLicenseActive + ", userInfo=" + userInfo
				+ "]";
	}

}
