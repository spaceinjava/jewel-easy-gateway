/**
 * 
 */
package com.spaceinje.gateway.validators;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;

import brave.Tracer;

/**
 * @author Lakshmi Kiran
 * @implNote contains the basic validation for email, mobile, password, pin code
 *           etc.
 * @version 1.0 {@value true or false}
 */
@Component("basicValidator")
@Singleton
public class BasicValidator {

	private static final Logger log = LoggerFactory.getLogger(BasicValidator.class);
	
	@Autowired
	private GatewayConstants constants;
	
	@Autowired
	private Tracer tracer;
	
	/**
	 * validating email with the describes regular
	 * expression @SearchConstants.EMAIL_REGEX
	 * 
	 * @param email {@value email of admin or tenant}
	 * @return {@value true or false}
	 */
	public boolean isValidEmail(String email) {
		if (log.isDebugEnabled())
			log.debug("isValidEmail service");
		if (StringUtils.isEmpty(email))
			return false;
		Pattern pattern = Pattern.compile(constants.getEmail_regex());

		if (log.isDebugEnabled())
			log.debug("Exiting isValidEmail service");
		return pattern.matcher(email).matches();
	}

	/**
	 * method to validate mobile number
	 * 
	 * @implNote The first digit should contain number between 7 to 9. The rest 9
	 *           digit can contain any number between 0 to 9. The mobile number can
	 *           have 11 digits also by including 0 at the starting. The mobile
	 *           number can be of 12 digits also by including 91 at the starting
	 *
	 * @param mobile - {@value mobile number or admin or tenant}
	 *
	 * @return {@value true or false}
	 */
	public boolean isMobileValid(String mobile) {
		if (log.isDebugEnabled())
			log.debug("Entering isMobileValid service");
		Pattern pattern = Pattern.compile(constants.getMobile_regex());
		if (StringUtils.isEmpty(mobile))
			return false;
		if (log.isDebugEnabled())
			log.debug("Exiting isMobileValid service");
		return pattern.matcher(mobile).matches();
	}
	
	/**
	 * method to validate the name whether it contains special characters or not and no spaces
	 * @param name {@value either first name or last name of admin ot tenant}
	 * @return {@value true or false}
	 */
	public boolean isNameValid(String name) {
		if (log.isDebugEnabled())
			log.debug("Entering isNameValid service");
		Pattern pattern = Pattern.compile(constants.getName_regex());
		if (StringUtils.isEmpty(name))
			return false;
		if (log.isDebugEnabled())
			log.debug("Exiting isNameValid service");
		return pattern.matcher(name).matches();
	}
	
	/**
	 * method to validate the password
	 * 
	 * @implNote It contains at least 8 characters and at most 20 characters. It
	 *           contains at least one digit. It contains at least one upper case
	 *           alphabet. It contains at least one lower case alphabet. It contains
	 *           at least one special character which includes !@#$%&*()-+=^. It
	 *           does not contain any white space.
	 *
	 * @param password - {@value login password of admin or tenant}
	 *
	 * @return {@value true or false}
	 */
	public boolean isPasswordValid(String password) {
		if (log.isDebugEnabled())
			log.debug("Entering isPasswordValid service");
		Pattern pattern = Pattern.compile(constants.getPassword_regex());
		// ^ represents starting character of the string.
		// (?=.*[0-9]) represents a digit must occur at least once.
		// (?=.*[a-z]) represents a lower case alphabet must occur at least once.
		// (?=.*[A-Z]) represents an upper case alphabet that must occur at least once.
		// (?=.*[@#$%^&-+=()] represents a special character that must occur at least
		// once.
		// (?=\\S+$) white spaces don’t allowed in the entire string.
		// {8, 20} represents at least 8 characters and at most 20 characters.
		// $ represents the end of the string.
		if (StringUtils.isEmpty(password))
			return false;
		if (log.isDebugEnabled())
			log.debug("Exiting isPasswordValid service");
		return pattern.matcher(password).matches();
	}

	/**
     * service to validate the password upon login
     *
     * @param originalPassword - used with login
     *
     * @param storedPassword - stored in database
     *
     * @return true or false
     *
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public boolean validateLoginPasswordMatch(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        if(log.isDebugEnabled())
            log.debug("Entering validateLoginPasswordMatch service");
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance(constants.getSecret_key_salt_algo());
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for(int i = 0; i < hash.length && i < testHash.length; i++){
            diff |= hash[i] ^ testHash[i];
        }
        if(log.isDebugEnabled())
            log.debug("Exiting validateLoginPasswordMatch service");
        return diff == 0;
    }
    
    private static byte[] fromHex(String hex){
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++){
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
    
    /**
	 * method to validate user sign in utils
	 * 
	 * @param util - SigninUtil
	 * 
	 * @return {@value ResponseEntity<ResponseCachet>}
	 */
	@SuppressWarnings("rawtypes")
	public ResponseEntity<ResponseCachet> validateSignInUtils(SigninUtil util, int request_type) {
		if (log.isDebugEnabled()) {
			log.debug("Entering validateSignInUtils method");
		}
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (null == util) {
			log.info("request can't be null");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("request can't be null");
			if (log.isDebugEnabled())
				log.debug("Exiting validateSignInUtils method");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (StringUtils.isEmpty(util.getUserName())) {
			log.info("user name can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("user name can't be empty");
			if (log.isDebugEnabled())
				log.debug("Exiting validateSignInUtils method");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (StringUtils.isEmpty(util.getPassword())) {
			log.info("password can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("password can't be empty");
			if (log.isDebugEnabled())
				log.debug("Exiting validateSignInUtils method");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (request_type == constants.getService_type_update()) {
			if (StringUtils.isEmpty(util.getNewPassword())) {
				log.info("new password can't be empty");
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("new password can't be empty");
				if (log.isDebugEnabled())
					log.debug("Exiting validateSignInUtils method");
				return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
			}
		}
		cachet.setStatus(constants.getStatus_message_success());
		cachet.setMessage("signin utils validated successfully");
		if (log.isDebugEnabled())
			log.debug("Exiting validateSignInUtils method");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
}
