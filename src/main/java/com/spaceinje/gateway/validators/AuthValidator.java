/**
 * 
 */
package com.spaceinje.gateway.validators;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;

import brave.Tracer;

/**
 * @author Lakshmi Kiran
 * @implNote validator class which is used to validate the params for
 *           authorization and authentication services
 * @version 1.0
 */
@SuppressWarnings({ "rawtypes" })
@Component
public class AuthValidator {

	private static final Logger log = LoggerFactory.getLogger(AuthValidator.class);

	@Autowired
	private GatewayConstants constants;

	@Autowired
	Tracer tracer;

	/**
	 * method which validates the authorization parameters below
	 * 
	 * @param client_id     - authorize a client based on valid client id
	 * @param tenant_secret - authorizing a tenant based on tenant secret
	 * @return {@value ResponseCachet} contains the validation status
	 */
	public ResponseEntity<ResponseCachet> isAuthParamsValid(String hash, String secret) {
		log.debug("Entering isAuthParamsValid service");
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(secret)) {
			cachet.setMessage("secret can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			log.debug("secret can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (StringUtils.isEmpty(hash)) {
			cachet.setMessage("hash can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			log.debug("hash can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setMessage("all auth params validated successfully");
		cachet.setStatus(constants.getStatus_message_success());
		log.debug("all auth params validated successfully");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * method to validate user sign in utils
	 * 
	 * @param util - SigninUtil
	 * 
	 * @return {@value ResponseEntity<ResponseCachet>}
	 */
	public ResponseEntity<ResponseCachet> validateSignInUtils(SigninUtil util, int request_type) {
		log.debug("Entering validateSignInUtils method");
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (request_type == constants.getService_type_update() && StringUtils.isEmpty(util.getNewPassword())) {
			log.debug("new password can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("new password can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getStatus_message_success());
		cachet.setMessage("signin utils validated successfully");
		log.debug("signin utils validated successfully");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
}
