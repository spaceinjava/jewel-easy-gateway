package com.spaceinje.gateway.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.spaceinje.gateway.bean.UserInfoBean;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class UserValidator {

	private static final Logger log = LoggerFactory.getLogger(UserValidator.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private GatewayConstants constants;

	@Autowired
	private BasicValidator basicValidator;

	public ResponseEntity<ResponseCachet> userValidator(UserInfoBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (bean.getIsTenant() && !basicValidator.isValidEmail(bean.getEmail())) {
			log.info("invalid email");
			cachet.setMessage("invalid email");
			cachet.setStatus(constants.getStatus_message_failure());
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (!basicValidator.isMobileValid(bean.getMobile())) {
			log.info("invalid mobile");
			cachet.setMessage("invalid mobile : The first digit should contain number between 6 to 9. The rest 9\r\n"
					+ "	 *           digit can contain any number between 0 to 9. The mobile number can\r\n"
					+ "	 *           have 11 digits also by including 0 at the starting. The mobile\r\n"
					+ "	 *           number can be of 12 digits also by including 91 at the starting");
			cachet.setStatus(constants.getStatus_message_failure());
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (!basicValidator.isNameValid(bean.getFirstName())) {
			log.info("invalid first name");
			cachet.setMessage("invalid first name : ignore case: \"(?i)\"\r\n"
					+ "can only start with an a-z character\r\n" + "can only end with an a-z\r\n"
					+ "is between 1 and 25 characters\r\n" + "can contain a-z and [ '-,.]\"");
			cachet.setStatus(constants.getStatus_message_failure());
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (!basicValidator.isNameValid(bean.getLastName())) {
			log.info("invalid last name");
			cachet.setMessage("invalid last name : ignore case: \"(?i)\"\r\n"
					+ "can only start with an a-z character\r\n" + "can only end with an a-z\r\n"
					+ "is between 1 and 25 characters\r\n" + "can contain a-z and [ '-,.]\"");
			cachet.setStatus(constants.getStatus_message_failure());
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getStatus_message_success());
		cachet.setMessage("userdetails validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
