package com.spaceinje.gateway.service;

import java.util.Set;

import com.spaceinje.gateway.bean.UserInfoBean;
import com.spaceinje.gateway.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface UserInfoService {

	public ResponseCachet saveOrUpdateUser(UserInfoBean bean);
	public ResponseCachet deleteUsers(Set<String> userIds);

}
