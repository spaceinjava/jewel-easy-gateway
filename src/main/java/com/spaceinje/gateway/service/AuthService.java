package com.spaceinje.gateway.service;

import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;

@SuppressWarnings("rawtypes")
public interface AuthService {

	public ResponseCachet authorize(String hash, String secret);

	public ResponseCachet login(SigninUtil util, String authCode);

	public ResponseCachet forgotPassword(String userName, String authCode);

	public ResponseCachet resetPassword(SigninUtil util, String authToken);
	
	public ResponseCachet findSessionByToken(String authToken);

}
