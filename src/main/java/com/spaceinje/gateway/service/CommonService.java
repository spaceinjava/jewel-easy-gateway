/**
 * 
 */
package com.spaceinje.gateway.service;

import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;

/**
 * @author Lakshmi Kiran
 *
 */
@SuppressWarnings("rawtypes")
public interface CommonService {

	public ResponseCachet resetPassword(SigninUtil util, boolean is_tenant);

}
