/**
 * 
 */
package com.spaceinje.gateway.service.nosql;

import com.spaceinje.gateway.model.nosql.UserInfo;

/**
 * @author Lakshmi Kiran
 *
 */
public interface UserInfoService {

	UserInfo findByHash(String hash);
}
