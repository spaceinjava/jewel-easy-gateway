/**
 * 
 */
package com.spaceinje.gateway.service.nosql;

import com.spaceinje.gateway.model.nosql.SessionTracker;
import com.spaceinje.gateway.util.ResponseCachet;

/**
 * @author Lakshmi Kiran
 *
 */
@SuppressWarnings("rawtypes")
public interface SessionCachetService {

	SessionTracker findByCode(String code);
	
	SessionTracker findByAuthToken(String auth_token);
	
	SessionTracker updateSession(SessionTracker cachet);
	
	ResponseCachet findSessionByAuth(String token);
	
	public boolean isSessionActive(SessionTracker cachet);
	
	public ResponseCachet inactivateSession(String auth_token);
}
