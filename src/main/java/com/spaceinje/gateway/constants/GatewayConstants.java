/**
 * 
 */
package com.spaceinje.gateway.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Lakshmi Kiran
 *
 */
@Component("constants")
public class GatewayConstants {

	
	@Value("${status.message.success}")
	String status_message_success;

	@Value("${status.message.failure}")
	String status_message_failure;
	
	@Value("${status.message.exception}")
	String status_message_exception;
	
	@Value("${secret.key.left.limit}")
	int secret_key_left_limit;
	
	@Value("${secret.key.right.limit}")
	int secret_key_right_limit;
	
	@Value("${secret.key.target.length}")
	int secret_key_target_length;
	
	@Value("${secret.key.salt.iterations}")
	int secret_key_salt_iterations;
	
	@Value("${secret.key.salt.algo}")
	String secret_key_salt_algorithm;
	
	@Value("${secret.key.salt.algo.instance}")
	String secret_key_salt_algo_instance;
	
	@Value("${user.auth.code.expiry}")
	int auth_code_expiry_in_seconds;
	
	@Value("${user.auth.token.expiry}")
	int auth_token_expiry_in_seconds;

	@Value("${status.response.type.code}")
	String status_response_type_code;
	
	@Value("${status.response.type.token}")
	String status_response_type_token;
	
	@Value("${status.response.type.password}")
	String status_response_type_password;
	
	@Value("${spring.mail.username}")
	String from_email;
	
	@Value("${secret.key.salt.algo}")
	String secret_key_salt_algo;
	
	@Value("${password.regex}")
	String password_regex;
	
	@Value("${alpha.capital.case}")
	String alpha_capital_case;

	@Value("${alpha.small.case}")
	String alpha_small_case;

	@Value("${special.chars}")
	String special_chars;

	@Value("${natural.numbers}")
	String natural_numbers;
	
	@Value("${forgot.password.subject}")
	String forgot_password_subject;
	
	@Value("${login.status.new}")
	String login_status_new;
	
	@Value("${login.status.activated}")
	String login_status_activated;
	
	@Value("${password.suffix}")
	String password_suffix;
	
	@Value("${max.login.attempts}")
	String max_login_attempts;
	
	@Value("${password.reset.url}")
	String password_reset_url;
	
	@Value("${admin.service.type.save}")
	int service_type_save;

	@Value("${admin.service.type.update}")
	int service_type_update;
	
	@Value("${email.regex}")
	String email_regex;

	@Value("${mobile.regex}")
	String mobile_regex;
	
	@Value("${name.regex}")
	String name_regex;
	
	@Value("${tenant.id.prefix}")
	String tenant_id_prefix;
	
	@Value("${id.length}")
	int id_length;
	
	@Value("${admin.email.subject}")
	String admin_email_subject;

	@Value("${tenant.email.subject}")
	String tenant_email_subject;
	
	@Value("${env.type}")
	String env_type;
	
	@Value("${license.months}")
	int licenseMonths;
	
	@Value("${pdf.file.location}")
	String pdf_location;
	
	public int getService_type_save() {
		return service_type_save;
	}

	public int getService_type_update() {
		return service_type_update;
	}

	public String getForgot_password_subject() {
		return forgot_password_subject;
	}

	public String getLogin_status_new() {
		return login_status_new;
	}

	public String getLogin_status_activated() {
		return login_status_activated;
	}

	public String getPassword_suffix() {
		return password_suffix;
	}

	public String getMax_login_attempts() {
		return max_login_attempts;
	}

	public String getPassword_reset_url() {
		return password_reset_url;
	}

	public String getAlpha_capital_case() {
		return alpha_capital_case;
	}

	public String getAlpha_small_case() {
		return alpha_small_case;
	}

	public String getSpecial_chars() {
		return special_chars;
	}

	public String getNatural_numbers() {
		return natural_numbers;
	}

	public String getSecret_key_salt_algo() {
		return secret_key_salt_algo;
	}

	public String getPassword_regex() {
		return password_regex;
	}

	public String getFrom_email() {
		return from_email;
	}

	/**
	 * @return the status_message_exception
	 */
	public String getStatus_message_exception() {
		return status_message_exception;
	}

	/**
	 * @return the status_response_type_code
	 */
	public String getStatus_response_type_code() {
		return status_response_type_code;
	}

	/**
	 * @return the status_response_type_token
	 */
	public String getStatus_response_type_token() {
		return status_response_type_token;
	}

	/**
	 * @return the status_response_type_password
	 */
	public String getStatus_response_type_password() {
		return status_response_type_password;
	}

	/**
	 * @return the status_message_success
	 */
	public String getStatus_message_success() {
		return status_message_success;
	}

	/**
	 * @return the status_message_failure
	 */
	public String getStatus_message_failure() {
		return status_message_failure;
	}

	
	/**
	 * @return the secret_key_left_limit
	 */
	public int getSecret_key_left_limit() {
		return secret_key_left_limit;
	}

	/**
	 * @return the secret_key_right_limit
	 */
	public int getSecret_key_right_limit() {
		return secret_key_right_limit;
	}

	/**
	 * @return the secret_key_target_length
	 */
	public int getSecret_key_target_length() {
		return secret_key_target_length;
	}

	/**
	 * @return the secret_key_salt_iterations
	 */
	public int getSecret_key_salt_iterations() {
		return secret_key_salt_iterations;
	}

	/**
	 * @return the secret_key_salt_algorithm
	 */
	public String getSecret_key_salt_algorithm() {
		return secret_key_salt_algorithm;
	}

	/**
	 * @return the secret_key_salt_algo_instance
	 */
	public String getSecret_key_salt_algo_instance() {
		return secret_key_salt_algo_instance;
	}

	/**
	 * @return the auth_code_expiry_in_seconds
	 */
	public int getAuth_code_expiry_in_seconds() {
		return auth_code_expiry_in_seconds;
	}

	/**
	 * @return the auth_token_expiry_in_seconds
	 */
	public int getAuth_token_expiry_in_seconds() {
		return auth_token_expiry_in_seconds;
	}

	public String getEmail_regex() {
		return email_regex;
	}

	public String getMobile_regex() {
		return mobile_regex;
	}

	public String getName_regex() {
		return name_regex;
	}

	public String getTenant_id_prefix() {
		return tenant_id_prefix;
	}

	public int getId_length() {
		return id_length;
	}

	public String getAdmin_email_subject() {
		return admin_email_subject;
	}

	public String getTenant_email_subject() {
		return tenant_email_subject;
	}

	public String getEnv_type() {
		return env_type;
	}

	public int getLicenseMonths() {
		return licenseMonths;
	}

	public String getPdf_location() {
		return pdf_location;
	}

	@Override
	public String toString() {
		return "GatewayConstants [status_message_success=" + status_message_success + ", status_message_failure="
				+ status_message_failure + ", status_message_exception=" + status_message_exception
				+ ", secret_key_left_limit=" + secret_key_left_limit + ", secret_key_right_limit="
				+ secret_key_right_limit + ", secret_key_target_length=" + secret_key_target_length
				+ ", secret_key_salt_iterations=" + secret_key_salt_iterations + ", secret_key_salt_algorithm="
				+ secret_key_salt_algorithm + ", secret_key_salt_algo_instance=" + secret_key_salt_algo_instance
				+ ", auth_code_expiry_in_seconds=" + auth_code_expiry_in_seconds + ", auth_token_expiry_in_seconds="
				+ auth_token_expiry_in_seconds + ", status_response_type_code=" + status_response_type_code
				+ ", status_response_type_token=" + status_response_type_token + ", status_response_type_password="
				+ status_response_type_password + ", from_email=" + from_email + ", secret_key_salt_algo="
				+ secret_key_salt_algo + ", password_regex=" + password_regex + ", alpha_capital_case="
				+ alpha_capital_case + ", alpha_small_case=" + alpha_small_case + ", special_chars=" + special_chars
				+ ", natural_numbers=" + natural_numbers + ", forgot_password_subject=" + forgot_password_subject
				+ ", login_status_new=" + login_status_new + ", login_status_activated=" + login_status_activated
				+ ", password_suffix=" + password_suffix + ", max_login_attempts=" + max_login_attempts
				+ ", password_reset_url=" + password_reset_url + ", service_type_save=" + service_type_save
				+ ", service_type_update=" + service_type_update + ", email_regex=" + email_regex + ", mobile_regex="
				+ mobile_regex + ", name_regex=" + name_regex + ", tenant_id_prefix=" + tenant_id_prefix
				+ ", id_length=" + id_length + ", admin_email_subject=" + admin_email_subject
				+ ", tenant_email_subject=" + tenant_email_subject + ", env_type=" + env_type + ", licenseMonths="
				+ licenseMonths + ", pdf_location=" + pdf_location + "]";
	}
}
