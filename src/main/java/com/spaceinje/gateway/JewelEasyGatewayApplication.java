package com.spaceinje.gateway;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.spaceinje.gateway.filters.ErrorFilter;
import com.spaceinje.gateway.filters.PostFilter;
import com.spaceinje.gateway.filters.PreFilter;
import com.spaceinje.gateway.filters.RouteFilter;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@EnableDiscoveryClient
@EnableZuulProxy
@RefreshScope
@OpenAPIDefinition(info = @Info(title = "Jewel Easy Gateway", description = "Gateway service for all the other services", version = "1.0"), servers = {
		@Server(url = "https://dev.spaceinje.com/gateway", description = "Dev Environment"),
		@Server(url = "http://localhost:9999", description = "Local") })
@SpringBootApplication(scanBasePackages = { "com.spaceinje.gateway" })
@EnableMongoRepositories(basePackages = { "com.spaceinje.gateway.repo" })
@EnableCaching
@EnableHystrix
@EnableWebSecurity
@EnableMongoAuditing
@EnableJpaRepositories(basePackages = { "com.spaceinje.gateway.repository" })
@EnableJpaAuditing
public class JewelEasyGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(JewelEasyGatewayApplication.class, args);
	}

	@Bean
	public PreFilter preFilter() {
		return new PreFilter();
	}

	@Bean
	public PostFilter postFilter() {
		return new PostFilter();
	}

	@Bean
	public ErrorFilter errorFilter() {
		return new ErrorFilter();
	}

	@Bean
	public RouteFilter routeFilter() {
		return new RouteFilter();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
