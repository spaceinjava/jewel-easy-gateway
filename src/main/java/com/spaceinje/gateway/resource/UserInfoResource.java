package com.spaceinje.gateway.resource;

import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.gateway.bean.UserInfoBean;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.service.UserInfoService;
import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.validators.UserValidator;

import brave.Tracer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/v1/gateway/user")
public class UserInfoResource {

	private static final Logger log = LoggerFactory.getLogger(UserInfoResource.class);

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private UserValidator userValidator;

	@Autowired
	private GatewayConstants constants;

	@Autowired
	private Tracer tracer;

	@ApiResponse(content = @Content(schema = @Schema(implementation = UserInfoBean.class)))
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateUser(@Valid @RequestBody UserInfoBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = userValidator.userValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating stone bean");
			return entity;
		}
		log.debug("calling saveOrUpdateUser service");
		ResponseCachet cachet = userInfoService.saveOrUpdateUser(bean);

		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateUser service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteUsers(@RequestParam(name = "userId") Set<String> userId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(userId)) {
			log.debug("userIds can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("userIds can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}

		cachet = userInfoService.deleteUsers(userId);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteUsers service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
