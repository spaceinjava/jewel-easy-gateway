package com.spaceinje.gateway.resource;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.service.AuthService;
import com.spaceinje.gateway.service.nosql.SessionCachetService;
import com.spaceinje.gateway.util.ResponseCachet;
import com.spaceinje.gateway.util.SigninUtil;
import com.spaceinje.gateway.validators.AuthValidator;

import brave.Tracer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/v1/gateway")
public class AuthResource {

	private static final Logger log = LoggerFactory.getLogger(AuthResource.class);

	@Autowired
	AuthValidator authValidator;

	@Autowired
	AuthService authFilterService;

	@Autowired
	GatewayConstants constants;

	@Autowired
	SessionCachetService sessionCachetService;

	@Autowired
	Tracer tracer;

	/**
	 * service to authorize
	 * 
	 * @param hash
	 * @param secret
	 * @return {@value ResponseEntity<ResponseCachet>} contains the validation or
	 *         authorization status
	 */
	@Operation
	@ApiResponse(description = "authorization service")
	@HystrixCommand(defaultFallback = "serviceFailure", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000") })
	@PostMapping(value = "/authorize")
	public ResponseEntity<ResponseCachet> authorize(@RequestHeader(value = "hash") String hash,
			@RequestHeader("secret") String secret) {
		ResponseEntity<ResponseCachet> entity = authValidator.isAuthParamsValid(hash, secret);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating authorize params");
			return entity;
		}
		log.debug("calling authorize service");
		ResponseCachet cachet = authFilterService.authorize(hash, secret);

		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting authorize service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * fallback method for the other services
	 * 
	 * @return {@value ResponseEntity}
	 */
	public ResponseEntity<ResponseCachet> serviceFailure() {
		if (log.isDebugEnabled())
			log.debug("Entering fallBackMethod ");
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		cachet.setMessage("No response from gateway service");
		cachet.setStatus(constants.getStatus_message_failure());

		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.GATEWAY_TIMEOUT);
	}

	@HystrixCommand(defaultFallback = "serviceFailure", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000") })
	@RequestMapping(value = "logout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> logout(@RequestParam(value = "authToken", required = true) String authToken) {
		if (log.isDebugEnabled())
			log.debug("Entering logout service");
		// calling session cachet service
		ResponseCachet cachet = sessionCachetService.inactivateSession(authToken);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			if (log.isDebugEnabled())
				log.debug("Exiting logout service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (log.isDebugEnabled())
			log.debug("Exiting logout service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service used to login
	 * 
	 * @param authCode
	 * @param util
	 * @return
	 * @throws MethodArgumentNotValidException
	 */
	@PostMapping(value = "/login")
	public ResponseEntity<ResponseCachet> login(@RequestHeader("authCode") String authCode,
			@Valid @RequestBody SigninUtil util) throws MethodArgumentNotValidException {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(authCode)) {
			log.debug("code can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		ResponseEntity<ResponseCachet> entity = authValidator.validateSignInUtils(util,
				constants.getService_type_save());
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating login params");
			return entity;
		}
		log.debug("calling login service");
		cachet = authFilterService.login(util, authCode);

		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting login service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service used to get the new password
	 * 
	 * @param authCode - authorization code used in the last login
	 * @param userName - userName (emailId) of the logging in user
	 * 
	 * @return {@value ResponseEntity<ResponseCachet>}
	 */
	@PostMapping(value = "/forgot/password", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> forgotPassword(@RequestHeader("authCode") String authCode,
			@RequestParam("userName") String userName) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(userName)) {
			log.debug("username can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("username can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (StringUtils.isEmpty(authCode)) {
			log.debug("code can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling forgotPassword service");
		cachet = authFilterService.forgotPassword(userName, authCode);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting forgotPassword service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to reset the password for admin or tenant
	 * 
	 * @param util  - sign in util
	 * 
	 * @param token - authentication token
	 * 
	 * @return {@value ResponseEntity<ResponseCachet>}
	 */
	@PostMapping(value = "/reset/password", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> resetPassword(@RequestBody SigninUtil util,
			@RequestHeader("authToken") String authToken) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(authToken)) {
			log.debug("authToken can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("authToken can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		ResponseEntity<ResponseCachet> entity = authValidator.validateSignInUtils(util,
				constants.getService_type_update());
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating reset password params");
			return entity;
		}
		log.debug("calling reset password service");
		cachet = authFilterService.resetPassword(util, authToken);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting resetPassword service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
