package com.spaceinje.gateway.specifications;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on isDeleted false
	 * 
	 * @return
	 */
	private static Specification isDeletedSpec() {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isDeleted"), Boolean.FALSE);
	}

	/**
	 * return criteria to retrieve based on given tenantCode
	 * 
	 * @param tenantCode
	 * @return
	 */
	private static Specification tenantCodeSpec(String tenantCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("tenantCode"), tenantCode);
	}

	/**
	 * return criteria to retrieve based on given shortCode
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification shortCodeSpec(String shortCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("shortCode"), shortCode);
	}

	/**
	 * return criteria to retrieve based on given email
	 * 
	 * @param email
	 * @return
	 */
	private static Specification emailSpec(String email) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("email"), email);
	}

	/**
	 * return criteria to retrieve based on given mobile
	 * 
	 * @param mobile
	 * @return
	 */
	private static Specification mobileSpec(String mobile) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("mobile"), mobile);
	}

	/**
	 * return criteria to retrieve based on given hashCode
	 * 
	 * @param hashCode
	 * @return
	 */
	private static Specification hashCodeSpec(String hashCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("hash"), hashCode);
	}

	/**
	 * return criteria to retrieve based on given userName
	 * 
	 * @param userName
	 * @return
	 */
	private static Specification userNameSpec(String userName) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("userName"), userName);
	}

	/**
	 * return criteria to retrieve based on isAuthCodeActive true
	 * 
	 * @param userName
	 * @return
	 */
	private static Specification isAuthCodeActiveSpec() {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isAuthCodeActive"), Boolean.TRUE);
	}

	/**
	 * return criteria to retrieve based on given authCode
	 * 
	 * @param authCode
	 * @return
	 */
	private static Specification authCodeSpec(String authCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("authCode"), authCode);
	}
	
	/**
	 * return criteria to retrieve based on isAuthTokenActive true
	 * 
	 * @param userName
	 * @return
	 */
	private static Specification isAuthTokenActiveSpec() {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isAuthTokenActive"), Boolean.TRUE);
	}

	/**
	 * return criteria to retrieve based on given authToken
	 * 
	 * @param authCode
	 * @return
	 */
	private static Specification authTokenSpec(String authToken) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("authToken"), authToken);
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and tenantCode</b>
	 * 
	 * @param tenantCode
	 * @return
	 */
	public static Specification findByTenantCodeSpec(String tenantCode) {
		return Specification.where(tenantCodeSpec(tenantCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and shortCode</b>
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification findByShortCodeSpec(String shortCode) {
		return Specification.where(shortCodeSpec(shortCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and email</b>
	 * 
	 * @param email
	 * @return
	 */
	public static Specification findByEmailSpec(String email) {
		return Specification.where(emailSpec(email)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false, tenantCode and
	 * shortCode</b>
	 * 
	 * @param tenantCode
	 * @param shortCode
	 * @return
	 */
	public static Specification findByTenantAndShortCodeSpec(String tenantCode, String shortCode) {
		return Specification.where(findByTenantCodeSpec(tenantCode)).and(findByShortCodeSpec(shortCode))
				.and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on email or mobile and isDeleted
	 * false</b>
	 * 
	 * @param email
	 * @param mobile
	 * @return
	 */
	public static Specification findByEmailOrMobileSpec(String email, String mobile) {
		return Specification.where(emailSpec(email)).or(mobileSpec(mobile)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and hashCode</b>
	 * 
	 * @param hashCode
	 * @return
	 */
	public static Specification findByHashCodeSpec(String hashCode) {
		return Specification.where(hashCodeSpec(hashCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and userName</b>
	 * 
	 * @param userName
	 * @return
	 */
	public static Specification findByUserNameSpec(String userName) {
		return Specification.where(userNameSpec(userName)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and isAuthCodeActive
	 * true</b>
	 * 
	 * @return
	 */
	public static Specification findAllActiveAuthCodesSpec() {
		return Specification.where(isAuthCodeActiveSpec()).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false, AuthCode and
	 * isAuthCode active</b>
	 * 
	 * @param authCode
	 * @return
	 */
	public static Specification findByAuthCodeSpec(String authCode) {
		return Specification.where(authCodeSpec(authCode)).and(findAllActiveAuthCodesSpec());
	}
	
	/**
	 * <b>creates criteria to retrieve based on isDeleted false and isAuthTokenActive
	 * true</b>
	 * 
	 * @return
	 */
	public static Specification findAllActiveAuthTokensSpec() {
		return Specification.where(isAuthTokenActiveSpec()).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false, authToken and
	 * isAuthToken active</b>
	 * 
	 * @param authCode
	 * @return
	 */
	public static Specification findByAuthTokenSpec(String authToken) {
		return Specification.where(authTokenSpec(authToken)).and(findAllActiveAuthTokensSpec());
	}


}
