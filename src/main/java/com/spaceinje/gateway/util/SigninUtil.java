package com.spaceinje.gateway.util;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NotNull
public class SigninUtil {

	@NotEmpty(message = "userName can't be null")
	private String userName;

	@NotEmpty(message = "password can't be null")
	private String password;

	private String newPassword;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public String toString() {
		return "SigninUtil [userName=" + userName + ", password=" + password + ", newPassword=" + newPassword + "]";
	}

}
