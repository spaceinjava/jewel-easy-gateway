/**
 * 
 */
package com.spaceinje.gateway.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Lakshmi Kiran
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCachet {
	
	@JsonInclude(Include.NON_NULL)
	private String client_id;
	
	@JsonInclude(Include.NON_NULL)
	private String client_secret;
	
	@JsonInclude(Include.NON_NULL)
	private String tenant_id;
	
	@JsonInclude(Include.NON_NULL)
	private String tenant_secret;

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getClient_secret() {
		return client_secret;
	}

	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTenant_secret() {
		return tenant_secret;
	}

	public void setTenant_secret(String tenant_secret) {
		this.tenant_secret = tenant_secret;
	}

	@Override
	public String toString() {
		return "UserCachet [client_id=" + client_id + ", client_secret=" + client_secret + ", tenant_id=" + tenant_id
				+ ", tenant_secret=" + tenant_secret + "]";
	}
	
	
}
