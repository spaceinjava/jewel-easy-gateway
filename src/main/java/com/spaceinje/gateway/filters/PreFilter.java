/**
 * 
 */
package com.spaceinje.gateway.filters;

import java.nio.charset.Charset;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.spaceinje.gateway.constants.GatewayConstants;
import com.spaceinje.gateway.service.AuthService;
import com.spaceinje.gateway.util.ResponseCachet;

import brave.Tracer;

/**
 * @author Lakshmi Kiran
 *
 */
public class PreFilter extends ZuulFilter {

	@Autowired
	private GatewayConstants constants;

	@Autowired
	Tracer tracer;

	@Value("${spring.security.user.name}")
	String username;

	@Value("${spring.security.user.password}")
	String password;

	@Autowired
	private AuthService authService;

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("ISO-8859-1")));
		String authValue = "Basic " + new String(encodedAuth);
		ctx.addZuulRequestHeader(HttpHeaders.AUTHORIZATION, authValue);

		String authToken = request.getHeader("token");
		ResponseCachet cachet = new ResponseCachet<>();
		if (StringUtils.isEmpty(authToken)) {
			ctx.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("token can't be empty");
			cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
					.replace("LazySpan(", ""));
			ctx.setResponseBody(cachet.toString());
			ctx.setSendZuulResponse(false);
			return null;
		}
		cachet = authService.findSessionByToken(authToken);
		if (!cachet.getStatus().equals(constants.getStatus_message_success())) {
			ctx.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("Access Denied");
			cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
					.replace("LazySpan(", ""));
			ctx.setResponseBody(cachet.toString());
			ctx.setSendZuulResponse(false);
			return null;
		}
		return null;
	}
}
