///**
// * 
// */
//package com.spaceinje.gateway.repository;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.spaceinje.gateway.model.SessionTracker;
//
//public interface SessionTrackerRepo extends JpaRepository<SessionTracker, String>{
//
//	SessionTracker findByAuthCodeAndIsCodeActiveTrue(String auth_code);
//	
//	SessionTracker findByHashAndIsCodeActiveTrue(String hash);
//
//	SessionTracker findByAuthTokenAndIsTokenActiveTrue(String auth_token);
//
//	List<SessionTracker> getInActiveCodeSessions(boolean status);
//
//	List<SessionTracker> getInActiveTokenSessions(boolean status);
//
//	
//	
//}
