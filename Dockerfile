FROM java:8
WORKDIR /
ADD target/jewel-easy-gateway*.jar jewel-easy-gateway.jar
EXPOSE 80
CMD ["java", "-jar", "jewel-easy-gateway.jar"]